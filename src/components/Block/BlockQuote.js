import React from 'react';
import { StyleSheet, View } from 'react-native';
import Text from '../Text/Text';

const BlockQuote = ({ text }) => {
  return (
    <View style={styles.blockquote}>
      <Text>
        {text}
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  blockquote: {
    borderLeftWidth: 5,
    paddingLeft: 10
  }
})

export default BlockQuote