import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import images from '../../assets/images/images';

const Block = ({ imageUrl, docName }) => (
  <View style={styles.blockContainer}>
    <Image source={images.testImage} style={styles.imageContainer}></Image>
    <Text>
      Tiểu hồng trần
    </Text>
  </View>
)

const styles = StyleSheet.create({
  blockContainer: {

  },
  imageContainer: {
    width: '100%',
    height: '100%'
  }
})
export default Block