import { useTheme } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Avatar } from 'react-native-elements';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import AntDesignIcons from 'react-native-vector-icons/AntDesign';
import BlockQuote from '../Block/BlockQuote';


const Message = ({ id, replyId, replyText, owner, message, ownerName, noAction, onReply }) => {
  const {
    dark, width, colors, margin, navbar, normalize,
  } = useTheme();

  const handleReply = () => {
    onReply(id);
  }

  return (
    <View style={[styles.messageContainer, owner && styles.ownerContainer]}>
      <View style={styles.avatarContainer}>
        {
          !owner && <Avatar
            rounded
            size={'medium'}
            source={{
              uri: 'https://ui-avatars.com/api/?rounded=true&name=' + ownerName,
            }}
          />
        }
      </View>
      <View style={[styles.infoContainer, owner && styles.ownerInfo]}>
        <View>
          <Text style={styles.name}>{ownerName}</Text>
          <Text style={[styles.time, owner && styles.textWhite]}>3 minutes ago</Text>
        </View>

        {replyId && <BlockQuote text={replyText} />}

        <Text style={[owner ? styles.textWhite : styles.textDart, replyId && {marginTop: 10}]}>
          {message}
        </Text>
        {
          !noAction && (
            <View style={styles.actionContainer}>
              <View style={styles.vlcContainer}>
                <TouchableOpacity
                  style={styles.actionButton}
                >
                  <View style={{ flexDirection: "row" }}>
                    <AntDesignIcons name={'like1'} size={18} color={'#000'} />
                    <Text
                      style={styles.actionText}
                    >
                      Like
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={handleReply}
                >
                  <View style={{ flexDirection: "row", marginRight: 10 }}>
                    <FontAwesome name={'reply'} size={18} color={'#000'} />

                    <Text
                      style={styles.actionText}
                    >
                      Reply
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.shareArea}>
                <TouchableOpacity
                >
                  <View style={{ flexDirection: "row" }}>
                    <FontAwesome name={'share'} color={'black'} size={15} style={{ marginRight: 5 }} />
                    <Text
                      style={styles.actionText}
                    >
                      Share
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          )
        }
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  messageContainer: {
    flexDirection: 'row',
    padding: 10
  },
  avatarContainer: {
    paddingTop: 30,
    flexDirection: 'column'
  },
  infoContainer: {
    height: '100%',
    width: '75%',
    padding: 10,
    marginLeft: 5,
    borderRadius: 20,
    backgroundColor: '#fff'
  },
  ownerInfo: {
    backgroundColor: '#0084FF',
  },
  ownerContainer: {
    flexDirection: 'row-reverse'
  },
  name: {

  },
  time: {
    color: '#000',
    paddingBottom: 5,
    opacity: 0.3
  },
  textWhite: {
    color: '#fff',
    opacity: 1
  },
  textDart: {
    color: '#000',
  },
  actionContainer: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  actionButton: {
    marginRight: 20
  },
  actionText: {
    color: 'black',
  },
  vlcContainer: {
    flexDirection: 'row',
    width: '60%',
    justifyContent: 'space-around'
  },
  shareContainer: {
    width: '40%',
    textAlign: 'right'
  }
})
export default Message;