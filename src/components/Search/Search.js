import React from 'react';
import { Button, Text, View } from 'react-native';
import { Overlay } from 'react-native-elements';

const Search = () => {

  return (
    <View>
      <Button title="Open Overlay" />
      <Overlay isVisible={true}>
        <Text>Hello from Overlay!</Text>
      </Overlay>
    </View>
  )
}
export default Search;