import React from 'react';
import { StyleSheet, View } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import AntDesign from 'react-native-vector-icons/AntDesign';

const Dropdown = ({
  onValueChange,
  items,
  fieldSelect,
  styleCmp,
  placeholder,
  defaultValue,
}) => {
  const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      fontSize: 16,
      paddingVertical: 12,
      paddingHorizontal: 10,
      borderWidth: 1,
      borderColor: 'gray',
      borderRadius: 4,
      color: '#fff',
      paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
      paddingHorizontal: 8,
      paddingVertical: 8,
      borderWidth: 0.5,
      borderColor: styleCmp && styleCmp.bdColor ? styleCmp.bdColor : '#f2f2f2',
      borderRadius: 8,
      color: styleCmp && styleCmp.color ? styleCmp.color : '#fff',
      fontSize: styleCmp && styleCmp.fontSize ? styleCmp.fontSize : 14,
      fontWeight: styleCmp && styleCmp.bold ? styleCmp.bold : 'normal',
      paddingRight: 30, // to ensure the text is never behind the icon
    },
  });

  return (
    <RNPickerSelect
      placeholder={{
        label: placeholder || 'Please select',
        inputLabel: placeholder || 'Please select',
      }}
      useNativeAndroidPickerStyle={true}
      style={{
        ...pickerSelectStyles,
        iconContainer: {
          top: 5,
          right: 12,
        },
        placeholder: {
          color: styleCmp && styleCmp.color ? styleCmp.color : '#f2f2f2',
        },
        viewContainer: {
          backgroundColor: '#fff',
          color: '#fff',
        },
      }}
      Icon={() => {
        return <AntDesign name={'caretdown'} color={'#fff'} />;
      }}
      value={defaultValue}
      useNativeAndroidPickerStyle={false}
      onValueChange={value => onValueChange(value)}
      items={items.map(it => ({
        label: fieldSelect ? it[fieldSelect] : it.name,
        value: it.id,
        color: '#000',
      }))}
    />
  );
};
export default Dropdown;
