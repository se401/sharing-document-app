import React, { useEffect, useRef, useState } from 'react';
import {
  Pressable,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
} from 'react-native';
import { useTheme, useNavigation } from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SearchBar from 'react-native-searchbar';
import Dropdown from './Dropdown/Dropdown';

const CustomHeader = ({
  onSearchChange,
  onSelectedItem,
  items,
  defaultValue,
}) => {
  const { width, normalize, status } = useTheme();
  const BOOKW = normalize(140, 180);
  const BOOKH = BOOKW * 1.5;
  const HEADER = normalize(width + status, 50);
  const [search, setSearch] = useState('');
  const [selected, setSelected] = useState(defaultValue || '');
  const [data, setData] = useState([]);
  const [show, setShow] = useState(false);
  const searchCmp = useRef(null);

  useEffect(() => {
    console.log('CustomHeader ~ selected', selected);
  }, []);

  const styles = StyleSheet.create({
    header: {
      position: 'relative',
      width,
      padding: 10,
      flexDirection: 'row',
      height: HEADER,
      backgroundColor: '#FF6347',
    },
    searchControl: {
      width: (width * 2) / 3,
      alignItems: 'center',
      height: '100%',
    },
    iconSearch: {
      alignItems: 'flex-end',
      width: width / 4,
    },
    searchBarContainer: {
      position: 'absolute',
      top: HEADER - 10,
      width: width / 2,
      left: 0,
      zIndex: 99,
    },
  });

  const updateSearch = search => {
    setSearch(search);
    onSearchChange(search);
  };

  const handleSelect = selected => {
    if (selected) {
      setSelected(selected);
      onSelectedItem(selected);
    }
  };

  const showSearchBar = () => {
    const newState = !show;
    setShow(newState);
    newState ? searchCmp.current.show() : searchCmp.current.hide();
  };

  if (items && selected && items.length > 0) {
    return (
      <>
        <View style={styles.header}>
          <View style={styles.searchControl}>
            <Dropdown
              onValueChange={handleSelect}
              items={items}
              defaultValue={selected}
            />
          </View>
          <View style={styles.iconSearch}>
            <Pressable onPress={showSearchBar}>
              <AntDesign name={'search1'} size={24} color={'#fff'} />
            </Pressable>
          </View>
        </View>
        <SearchBar
          ref={searchCmp}
          placeholder={'Search here'}
          handleSearch={updateSearch}
          data={data}></SearchBar>
      </>
    );
  } else return <></>;
};
export default CustomHeader;
