import React, { useEffect } from 'react';
import { Image, LayoutAnimation, Pressable, StyleSheet, View } from 'react-native';
import { useTheme, useNavigation } from '@react-navigation/native';
import Animated, { Extrapolate, interpolate, useAnimatedStyle, useDerivedValue, useSharedValue, withTiming } from 'react-native-reanimated';
import { SharedElement } from 'react-navigation-shared-element';
import Text from '../Text/Text';


const BookV2 = ({ book, scrollX, index, hideTitle = false }) => {

  const navigation = useNavigation();
  const { margin, normalize } = useTheme();
  const BOOKW = normalize(130, 160);
  const BOOKH = BOOKW * 1.5;
  const position = useDerivedValue(() => (index + 0.00001) * (BOOKW + margin) - scrollX.value);
  const inputRange = [-BOOKW, 0, BOOKW, BOOKW * 3];
  const loaded = useSharedValue(0);

  useEffect(() => {
    LayoutAnimation.easeInEaseOut();
    loaded.value = withTiming(1);
  }, []);

  const bookDetails = () => {
    navigation.navigate('BookDetailV2', { postId: book.id });
  };

  const anims = {
    book: useAnimatedStyle(() => ({
      opacity: loaded.value,
      transform: [
        { perspective: 800 },
        { scale: interpolate(position.value, inputRange, [0.9, 1, 1, 1], Extrapolate.CLAMP) },
        { rotateY: `${interpolate(position.value, inputRange, [60, 0, 0, 0], Extrapolate.CLAMP)}deg` },
        {
          translateX: scrollX.value
            ? interpolate(position.value, inputRange, [BOOKW / 4, 0, 0, 0], 'clamp')
            : interpolate(loaded.value, [0, 1], [index * BOOKW, 0], 'clamp'),
        },
      ],
    })),
  };

  const styles = StyleSheet.create({
    imgBox: {
      marginRight: margin,
      borderRadius: 10,
      shadowRadius: 3,
      shadowOpacity: 0.3,
      shadowOffset: { width: 3, height: 3 },
    },
    bookImg: {
      width: BOOKW,
      height: BOOKH,
      borderRadius: 10,
    },
    bookText: {
      marginRight: margin,
      marginTop: margin / 2,
    },
    textWidth: {
      width: BOOKW + 50
    }
  });


  return (
    <Pressable onPress={bookDetails}>
      <Animated.View>
        <SharedElement id={book.id}>
          <View style={styles.imgBox}>
            <Image style={styles.bookImg} source={{ uri: book.backgroudUrl || 'https://ui-avatars.com/api/?rounded=true&name=' + book.title }} />
          </View>
        </SharedElement>
        <Text size={17} style={styles.textWidth}>{!hideTitle ? book.title : ''}</Text>
      </Animated.View>
    </Pressable>
  )
}

export default React.memo(BookV2);