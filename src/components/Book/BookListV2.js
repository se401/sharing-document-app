import { useNavigation, useTheme } from '@react-navigation/native';
import React from 'react';
import { FlatList, Pressable, StyleSheet, View } from 'react-native';
import Animated, {
  useAnimatedScrollHandler,
  useSharedValue,
} from 'react-native-reanimated';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BText from '../Text/Text';
import BookV2 from './BookV2';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const BookListV2 = ({ books, title, horizontal = true, handleLoadMore }) => {
  const { width, margin, colors } = useTheme();
  const navigation = useNavigation();
  const scrollX = useSharedValue(0);

  const scrollHandler = useAnimatedScrollHandler({
    onScroll: ({ contentOffset }) => {
      scrollX.value = contentOffset.x;
    },
  });

  const searchScreen = () => {
    navigation.push('BookSearch', {
      bookList: books,
    });
  };

  const styles = StyleSheet.create({
    list: {
      backgroundColor: colors.card,
      paddingTop: title === 'Reading' ? margin : 0,
    },
    heading: {
      paddingTop: margin,
      paddingHorizontal: margin,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    listContainer: {
      padding: margin,
    },
    emptyContainer: {
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      width: width - margin * 2,
      paddingVertical: margin * 2,
      backgroundColor: colors.background,
    },
    emptyText: {
      padding: margin,
    },
  });

  const EmptyList = () => (
    <Pressable onPress={searchScreen} style={styles.emptyContainer}>
      <AntDesign color={colors.text} size={27} name="book" />
      <BText size={16} center style={styles.emptyText}>
        {'Không có bài đăng nào.'}
      </BText>
    </Pressable>
  );

  const onLoadMore = handleLoadMore ? { onEndReached: handleLoadMore } : null;

  return (
    <View style={styles.list}>
      {horizontal && (
        <>
          <View style={styles.heading}>
            <BText size={17} bold>
              {title}
            </BText>
            <BText size={17}>{books.length}</BText>
          </View>
          <AnimatedFlatList
            {...onLoadMore}
            horizontal
            onScroll={scrollHandler}
            scrollEventThrottle={8}
            showsHorizontalScrollIndicator={horizontal}
            contentContainerStyle={styles.listContainer}
            data={books}
            keyExtractor={i => i.id.toString()}
            renderItem={({ item, index }) => (
              <BookV2
                book={item}
                index={index}
                scrollX={scrollX}
                navigation={navigation}
              />
            )}
            ListEmptyComponent={<EmptyList />}
          />
        </>
      )}
      {!horizontal && (
        <AnimatedFlatList
          numColumns={2}
          onScroll={scrollHandler}
          scrollEventThrottle={8}
          showsHorizontalScrollIndicator={horizontal}
          contentContainerStyle={styles.listContainer}
          data={books}
          keyExtractor={i => i.id}
          renderItem={({ item, index }) => (
            <BookV2
              book={item}
              index={index}
              scrollX={scrollX}
              navigation={navigation}
            />
          )}
          ListEmptyComponent={<EmptyList />}
        />
      )}
    </View>
  );
};

export default BookListV2;
