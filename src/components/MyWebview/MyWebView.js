import React from 'react';
import WebView from 'react-native-webview';

const MyWebView = ({ uri, styleWeb }) => {
  return (
    <WebView
      source={{ uri: uri }}
      style={[{ width: '100%', height: '100%' }]}
    />
  );
};
export default MyWebView;
