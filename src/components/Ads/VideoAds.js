import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Video from 'react-native-video';
import FontawesomeIcons from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Modalize } from 'react-native-modalize';
import { Button } from 'react-native-share';

export default class VideoAds extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      paused: false,
      currentTime: 0,
      currentTimeSkip: 0,
      fullscreen: true,
      count: 0,
    };
  }

  componentDidMount() {}
  componentWillUnmount() {
    console.log('VideoAds ~ componentWillUnmount ~ componentWillUnmount');
  }

  openModal() {
    this.sheetRef.open();
  }

  onAdsLoaded() {
    setTimeout(() => {
      this.state.paused = false;
    }, 10000);
  }

  onAdStarted(event) {
    this.setState({ paused: false });
  }

  onAdsComplete(event) {
    this.props.onAdsSuccess && this.props.onAdsSuccess();
    this.sheetRef.close();
  }

  onProcess(event) {
    // {
    //   currentTime: 5.2,
    //   playableDuration: 34.6,
    //   seekableDuration: 888
    // }
    this.setState({
      currentTime: (event.seekableDuration - event.currentTime).toFixed(0),
      currentTimeSkip: event.currentTime.toFixed(0),
    });
  }

  onBuffer(event) {
    console.log('VideoAds ~ onBuffer ~ event', event);
  }

  onClose() {
    this.sheetRef.close();
    this.props.onClose && this.props.onClose();
    if (this.state.currentTimeSkip > 10) {
      this.props.onAdsSuccess && this.props.onAdsSuccess();
    }
  }

  renderView() {
    return (
      <>
        <TouchableOpacity
          onPress={() => this.openModal()}
          style={styles.addButton}
          title="Press">
          <AntDesign size={21} name={'notification'} style={styles.addIcon} />
        </TouchableOpacity>
        <Modalize
          style={styles.container}
          ref={cmp => (this.sheetRef = cmp)}
          modalTopOffset={0}>
          <View style={{ width: '100%', height: '100%' }}>
            <View style={styles.countDownContainer}>
              <FontawesomeIcons
                name="close"
                size={18}
                color={'black'}
                style={{ marginTop: 4 }}
                onPress={() => this.onClose()}
              />
              <Text style={styles.countDown}>
                {this.state.currentTime} Để thoát
              </Text>
            </View>
            <Video
              ref={component => (this._video = component)}
              source={{
                uri: 'http://techslides.com/demos/sample-videos/small.mp4',
              }}
              style={styles.video}
              fullscreen={this.state.fullscreen}
              paused={false}
              onProgress={e => this.onProcess(e)}
              onVideoLoad={e => this.onAdsLoaded(e)}
              onVideoLoadStart={e => this.onAdStarted(e)}
              onEnd={e => this.onAdsComplete(e)}></Video>
          </View>
        </Modalize>
      </>
    );
  }

  render() {
    return this.renderView();
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    position: 'absolute',
    zIndex: 1000,
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  video: {
    width: '100%',
    height: 300,
    flex: 0.9,
  },
  countDownContainer: {
    width: '100%',
    paddingRight: 15,
    paddingTop: 15,
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  countDown: {
    textAlign: 'right',
    fontSize: 18,
    paddingLeft: 20,
  },
  addButton: {
    width: 60,
    height: 60,
    right: 10,
    bottom: 10,
    borderRadius: 60,
    position: 'absolute',
    backgroundColor: '#f78a37',
    zIndex: 101,
  },
  addIcon: {
    top: '35%',
    left: '35%',
    color: '#fff',
  },
});
