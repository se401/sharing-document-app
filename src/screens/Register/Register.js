import AsyncStorage from '@react-native-async-storage/async-storage';
import { Link } from '@react-navigation/native';
import React, { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import {
  ActivityIndicator,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Animated from 'react-native-reanimated';
import { register } from '../../api/services/auth.service';
import uuidv4 from '../../utils/uuid';

const Register = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [password, setPassword] = useState('');
  const [phone, setPhone] = useState('');
  const [displayName, setDisplayName] = useState('');
  const { handleSubmit, control, setValue } = useForm();

  let registerSubmit = data => {
    const username = uuidv4();
    setLoading(true);
    setPassword(data.password);
    setDisplayName(data.displayName);
    setPhone(data.phone);
    register(
      data.email,
      data.password,
      data.displayName,
      username,
      data.phone,
      '',
    )
      .then(val => {
        setLoading(false);
        AsyncStorage.setItem('@verifyToken', val.data.payload.tokenVerify).then(
          () => navigation.navigate('Verify'),
        );
      })
      .catch(e => {
        setLoading(false);
        console.log('register', JSON.stringify(e));
      });
  };

  let handlePasswordChange = (field, text) => {
    setPassword(text);
    field.onChange(text);
  };

  return (
    <SafeAreaView>
      {loading && (
        <ActivityIndicator
          color={'red'}
          size={'large'}
          style={{
            position: 'absolute',
            zIndex: 1000,
            backgroundColor: '#cccccc',
            width: '100%',
            height: '100%',
            opacity: 0.3
          }}
        />
      )}
      <Animated.ScrollView>
        <View style={[styles.loginContainer]}>
          <View style={styles.horizontal}>
            <View>
              <View style={styles.viewControl}>
                <Text style={styles.title}>
                  Email <Text style={{ color: 'red' }}>*</Text>:
                </Text>
                <Controller
                  name="email"
                  control={control}
                  defaultValue={null}
                  rules={{
                    required: {
                      value: true,
                      message: 'Email bắt buộc',
                    },
                    pattern: {
                      value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
                      message: 'Email sai định dạng',
                    },
                  }}
                  render={({ value, field, fieldState }) => (
                    <View>
                      <TextInput
                        onChangeText={text => field.onChange(text)}
                        value={value}
                        style={[
                          styles.input,
                          {
                            borderColor: fieldState.invalid
                              ? '#f44336'
                              : 'black',
                          },
                        ]}
                        placeholder="Nhập email"
                        placeholderTextColor={'#333'}
                      />
                      {fieldState.invalid && (
                        <Text style={{ color: '#f44336' }}>
                          {fieldState.error?.message}
                        </Text>
                      )}
                    </View>
                  )}
                />
              </View>
              <View style={styles.viewControl}>
                <Text style={styles.title}>
                  Mật khẩu <Text style={{ color: 'red' }}>*</Text>:
                </Text>
                <Controller
                  name="password"
                  control={control}
                  defaultValue={null}
                  rules={{
                    required: {
                      value: true,
                      message: 'Mật khẩu bắt buộc',
                    },
                  }}
                  render={({ value, field, fieldState }) => (
                    <View>
                      <TextInput
                        onChangeText={text => handlePasswordChange(field, text)}
                        secureTextEntry
                        value={value}
                        style={[
                          styles.input,
                          {
                            borderColor: fieldState.invalid
                              ? '#f44336'
                              : 'black',
                          },
                        ]}
                        placeholder="Nhập mật khẩu"
                        placeholderTextColor={'#333'}
                      />
                      {fieldState.invalid && (
                        <Text style={{ color: '#f44336' }}>
                          {fieldState.error?.message}
                        </Text>
                      )}
                    </View>
                  )}
                />
              </View>
              <View style={styles.viewControl}>
                <Text style={styles.title}>
                  Xác nhận Mật khẩu <Text style={{ color: 'red' }}>*</Text>:
                </Text>
                <Controller
                  name="confirmPassword"
                  control={control}
                  defaultValue={null}
                  rules={{
                    required: {
                      value: true,
                      message: 'Mật khẩu xác nhận bắt buộc',
                    },
                    validate: value =>
                      value === password || 'Mật khẩu không trùng khớp',
                  }}
                  render={({ value, field, fieldState }) => (
                    <View>
                      <TextInput
                        onChangeText={text => field.onChange(text)}
                        secureTextEntry
                        value={value}
                        style={[
                          styles.input,
                          {
                            borderColor: fieldState.invalid
                              ? '#f44336'
                              : 'black',
                          },
                        ]}
                        placeholder="Nhập xác nhận mật khẩu"
                        placeholderTextColor={'#333'}
                      />
                      {fieldState.invalid && (
                        <Text style={{ color: '#f44336' }}>
                          {fieldState.error?.message}
                        </Text>
                      )}
                    </View>
                  )}
                />
              </View>
              <View style={styles.viewControl}>
                <Text style={styles.title}>
                  Tên hiển thị <Text style={{ color: 'red' }}>*</Text>:
                </Text>
                <Controller
                  name="displayName"
                  control={control}
                  defaultValue={null}
                  rules={{
                    required: {
                      value: true,
                      message: 'Tên hiển thị bắt buộc',
                    },
                  }}
                  render={({ value, field, fieldState }) => (
                    <View>
                      <TextInput
                        onChangeText={text => field.onChange(text)}
                        value={value}
                        style={[
                          styles.input,
                          {
                            borderColor: fieldState.invalid
                              ? '#f44336'
                              : 'black',
                          },
                        ]}
                        placeholder="Nhập tên"
                        placeholderTextColor={'#333'}
                      />
                      {fieldState.invalid && (
                        <Text style={{ color: '#f44336' }}>
                          {fieldState.error?.message}
                        </Text>
                      )}
                    </View>
                  )}
                />
              </View>
              <View style={styles.viewControl}>
                <Text style={styles.title}>
                  Số điện thoại <Text style={{ color: 'red' }}>*</Text>:
                </Text>
                <Controller
                  name="phone"
                  control={control}
                  defaultValue={null}
                  rules={{
                    required: {
                      value: true,
                      message: 'Số điện thoại bắt buộc',
                    },
                  }}
                  render={({ value, field, fieldState }) => (
                    <View>
                      <TextInput
                        onChangeText={text => field.onChange(text)}
                        value={value}
                        style={[
                          styles.input,
                          {
                            borderColor: fieldState.invalid
                              ? '#f44336'
                              : 'black',
                          },
                        ]}
                        placeholder="Nhập số điện thoại"
                        placeholderTextColor={'#333'}
                      />
                      {fieldState.invalid && (
                        <Text style={{ color: '#f44336' }}>
                          {fieldState.error?.message}
                        </Text>
                      )}
                    </View>
                  )}
                />
              </View>
              <TouchableOpacity
                style={styles.btnContainer}
                onPress={handleSubmit(registerSubmit)}>
                <Text style={styles.text}>Register</Text>
              </TouchableOpacity>
              <Link style={styles.linkContainer} to={'/Login'}>
                Login
              </Link>
            </View>
          </View>
        </View>
      </Animated.ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  text: {
    color: '#000',
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
  },
  input: {
    width: 300,
    borderWidth: 2,
    color: '#333',
    borderRadius: 10,
  },
  loginContainer: {
    backgroundColor: '#c3c3c3',
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
    backgroundColor: '#fff',
  },
  imageContainer: {
    width: '100%',
    height: '100%',
  },
  viewControl: {
    flexDirection: 'column',
    width: '100%',

    marginTop: 10,
    marginBottom: 10,
    padding: 5,
  },
  title: {
    fontSize: 30,
    color: '#333',
    fontWeight: 'bold',
  },
  btnContainer: {
    padding: 10,
    backgroundColor: '#0000ff',
    color: '#333',
    borderRadius: 10,
    height: 50,
  },
  linkContainer: {
    textAlign: 'center',
    margin: 10,
    fontSize: 18,
  },
});

export default Register;
