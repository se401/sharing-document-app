import { useFocusEffect, useTheme } from '@react-navigation/native';
import React, { useCallback, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import {
  ActivityIndicator,
  Alert,
  Animated,
  PermissionsAndroid,
  SafeAreaView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { useSharedValue } from 'react-native-reanimated';
import { getActiveCategories } from '../../../api/services/category.service';
import { getActiveLevel } from '../../../api/services/level.service';
import { createPost } from '../../../api/services/post.service';
import { getActiveResource } from '../../../api/services/resource.service';
import Dropdown from '../../../components/Dropdown/Dropdown';
import Text from '../../../components/Text/Text';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import { Pressable } from 'react-native';
import Button from '../../../components/Button/Button';
import { convertCurrency } from '../../../api/services/currency.service';

const CreatePost = ({ navigation, isCreate }) => {
  const currencies = [
    {
      name: 'USD',
      id: 'USD',
    },
    {
      name: 'VND',
      id: 'VND',
    },
  ];

  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  const [post, setPost] = useState({});
  const [category, setCategory] = useState(null);
  const [categoryList, setCategoryList] = useState(null);
  const [level, setLevel] = useState(null);
  const [levelList, setLevelList] = useState(null);
  const [resource, setResource] = useState(null);
  const [resourceList, setResourceList] = useState(null);
  const [filePath, setFilePath] = useState(null);
  const [document, setDocument] = useState(null);
  const [loading, setLoading] = useState(true);
  const loaded = useSharedValue(0);

  const [currency, setCurrency] = useState('VND');

  const { handleSubmit, control, setValue } = useForm();
  const { height } = useTheme();

  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    middle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    text: {
      color: '#000',
      fontSize: 18,
      color: '#fff',
      textAlign: 'center',
    },
    input: {
      width: 300,
      color: '#333',
      borderWidth: 1,
      borderRadius: 10,
      borderColor: 'black',
    },
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      backgroundColor: '#fff',
    },
    imageContainer: {
      width: '100%',
      height: '100%',
    },
    viewControl: {
      flexDirection: 'column',
      width: '100%',
      marginTop: 7,
      marginBottom: 7,
    },
    title: {
      fontSize: 27,
      color: '#333',
      fontWeight: 'bold',
    },
    btnContainer: {
      padding: 10,
      backgroundColor: '#0000ff',
      color: '#333',
      borderRadius: 10,
    },
    btnContainerCancel: {
      padding: 10,
      backgroundColor: '#c3c3c3',
      color: '#333',
      borderRadius: 10,
      marginRight: 10,
    },
    textStyle: {
      padding: 10,
      color: 'black',
      textAlign: 'center',
    },
    buttonStyle: {
      alignItems: 'center',
      backgroundColor: '#DDDDDD',
      padding: 5,
      marginVertical: 10,
      width: '100%',
    },
    imageStyle: {
      width: 200,
      height: 200,
      margin: 5,
    },
  });

  const selectOneFile = async (type, field) => {
    //Opening Document Picker for selection of one file
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
        //There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      //Printing the log realted to the file
      console.log('res : ' + JSON.stringify(res));
      console.log('URI : ' + res.uri);
      console.log('Type : ' + res.type);
      console.log('File Name : ' + res.name);
      console.log('File Size : ' + res.size);
      //Setting the state to show single file attributes
      field.onChange(res);
      setDocument(res);
      setFilePath(res);
    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        alert('Canceled from single doc picker');
      } else {
        //For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  const chooseFile = (type, field) => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      includeBase64: true,
    };
    launchImageLibrary(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        return;
      } else if (response.errorCode == 'permission') {
        return;
      } else if (response.errorCode == 'others') {
        return;
      }
      console.log('base64 -> ', response.assets[0].base64);
      console.log('uri -> ', response.assets[0].uri);
      console.log('width -> ', response.assets[0].width);
      console.log('height -> ', response.assets[0].height);
      console.log('fileSize -> ', response.assets[0].fileSize);
      console.log('type -> ', response.assets[0].type);
      console.log('fileName -> ', response.assets[0].fileName);

      console.log('file', response.assets);
      field.onChange(response.assets[0]);
      setFilePath(response.assets[0]);
    });
  };

  const loadData = useCallback(async () => {
    const resourceRes = (await getActiveResource(10, 0)).data;
    const categoryRes = (await getActiveCategories(10, 0)).data;
    const levelRes = (await getActiveLevel(10, 0)).data;

    setCategoryList(categoryRes.payload);
    setLevelList(levelRes.payload);
    setResourceList(resourceRes.payload);
    setLoading(false);
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  );

  const handleBack = () => navigation.goBack();

  const onSubmit = data => {
    console.log(data, 'data');
    createPost(data, filePath).then(res => {
      if (res.status === 200 && res.data.statusCode === 200) {
        Alert.alert(
          'Thành công',
          'Đăng tài liệu thành công. Chờ xét duyệt!!!',
          [
            {
              text: 'Hủy',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: 'Xác nhận',
              onPress: () => navigation.navigate('Home'),
            },
          ],
          {
            cancelable: true,
          },
        );
      }
    });
  };

  const handleCurrencyChange = e => {
    console.log('handlePriceChange ~ e', e);
    if (e && e !== currency) {
      setCurrency(e);
    }
  };

  const handleBeforeChange = (text, field) => {
    console.log('handleBeforeChange ~ text', +text);
    setTimeout(() => {
      convertCurrency(currency, +text).then(res => {
        if (res.data.statusCode === 200) {
          field.onChange(res.data.payload.toFixed(5))
        }
      });
    }, 700);
  };

  const Loading = () => (
    <View style={styles.middle}>
      <ActivityIndicator size="large" color={'red'} />
    </View>
  );
  if (!loading) {
    return (
      <SafeAreaView>
        <Animated.ScrollView>
          <View style={styles.container}>
            <View style={styles.horizontal}>
              <View>
                <View style={styles.viewControl}>
                  <Text style={styles.title}>
                    Tiêu đề <Text style={{ color: 'red' }}>*</Text>:
                  </Text>
                  <Controller
                    name="title"
                    control={control}
                    defaultValue={null}
                    rules={{
                      required: { value: true, message: 'Tiêu đề là bắt buộc' },
                    }}
                    render={({ value, field, fieldState }) => (
                      <View>
                        <TextInput
                          onChangeText={text => field.onChange(text)}
                          value={value}
                          style={[
                            styles.input,
                            {
                              borderColor: fieldState.invalid
                                ? '#f44336'
                                : 'black',
                            },
                          ]}
                          placeholder="Nhập tiêu đề"
                          placeholderTextColor={'#333'}
                        />
                        {fieldState.invalid && (
                          <Text style={{ color: '#f44336' }}>
                            {fieldState.error?.message}
                          </Text>
                        )}
                      </View>
                    )}
                  />
                </View>
                <View style={styles.viewControl}>
                  <Text style={styles.title}>
                    Mô tả <Text style={{ color: 'red' }}>*</Text>:
                  </Text>
                  <Controller
                    name="description"
                    control={control}
                    defaultValue={null}
                    rules={{
                      required: { value: true, message: 'Mô tả là bắt buộc' },
                    }}
                    render={({ value, field, fieldState }) => (
                      <View>
                        <TextInput
                          onChangeText={text => field.onChange(text)}
                          value={value}
                          style={[
                            styles.input,
                            {
                              borderColor: fieldState.invalid
                                ? '#f44336'
                                : 'black',
                            },
                          ]}
                          placeholder="Nhập mô tả"
                          placeholderTextColor={'#333'}
                        />
                        {fieldState.invalid && (
                          <Text style={{ color: '#f44336' }}>
                            {fieldState.error?.message}
                          </Text>
                        )}
                      </View>
                    )}
                  />
                </View>
                <View style={styles.viewControl}>
                  <Text style={styles.title}>
                    Mục <Text style={{ color: 'red' }}>*</Text>:
                  </Text>
                  <Controller
                    name="category"
                    control={control}
                    defaultValue={null}
                    rules={{
                      required: {
                        value: true,
                        message: 'Danh mục là bắt buộc',
                      },
                    }}
                    render={({ value, field, fieldState }) => (
                      <View>
                        <Dropdown
                          styleCmp={{
                            color: 'black',
                            bdColor: fieldState.invalid ? '#f44336' : 'black',
                          }}
                          items={categoryList}
                          onValueChange={value => field.onChange(value)}
                          placeholder={'Chọn mục'}></Dropdown>
                        {fieldState.invalid && (
                          <Text style={{ color: '#f44336' }}>
                            {fieldState.error?.message}
                          </Text>
                        )}
                      </View>
                    )}
                  />
                </View>
                <View style={styles.viewControl}>
                  <Text style={styles.title}>
                    Mức độ <Text style={{ color: 'red' }}>*</Text>:
                  </Text>
                  <Controller
                    name="level"
                    control={control}
                    defaultValue={null}
                    rules={{
                      required: { value: true, message: 'Mức độ là bắt buộc' },
                    }}
                    render={({ value, field, fieldState }) => (
                      <View>
                        <Dropdown
                          styleCmp={{
                            color: 'black',
                            bdColor: fieldState.invalid ? '#f44336' : 'black',
                          }}
                          items={levelList}
                          onValueChange={value => field.onChange(value)}
                          placeholder={'Chọn mức độ'}></Dropdown>
                        {fieldState.invalid && (
                          <Text style={{ color: '#f44336' }}>
                            {fieldState.error?.message}
                          </Text>
                        )}
                      </View>
                    )}
                  />
                </View>
                <View style={styles.viewControl}>
                  <Text style={styles.title}>
                    Nguồn <Text style={{ color: 'red' }}>*</Text>:
                  </Text>
                  <Controller
                    name="resource"
                    control={control}
                    defaultValue={null}
                    rules={{
                      required: { value: true, message: 'Nguồn là bắt buộc' },
                    }}
                    render={({ value, field, fieldState }) => (
                      <View>
                        <Dropdown
                          styleCmp={{
                            color: 'black',
                            bdColor: fieldState.invalid ? '#f44336' : 'black',
                          }}
                          items={resourceList}
                          onValueChange={value => field.onChange(value)}
                          placeholder={'Chọn nguồn'}></Dropdown>
                        {fieldState.invalid && (
                          <Text style={{ color: '#f44336' }}>
                            {fieldState.error?.message}
                          </Text>
                        )}
                      </View>
                    )}
                  />
                </View>
                <View style={styles.viewControl}>
                  <Text style={styles.title}>
                    Link tài liệu <Text style={{ color: 'red' }}>*</Text>:
                  </Text>
                  <Controller
                    name="source"
                    control={control}
                    defaultValue={null}
                    rules={{
                      required: {
                        value: true,
                        message: 'Tài liệu là bắt buộc',
                      },
                      pattern: {
                        value:
                          /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/,
                        message: 'Sai định dạng đường dẫn',
                      },
                    }}
                    render={({ value, field, fieldState }) => (
                      <View>
                        <TextInput
                          onChangeText={text => field.onChange(text)}
                          value={value}
                          style={[
                            styles.input,
                            {
                              borderColor: fieldState.invalid
                                ? '#f44336'
                                : 'black',
                            },
                          ]}
                          placeholder="Nhập link tài liệu"
                          placeholderTextColor={'#333'}
                        />
                        {fieldState.invalid && (
                          <Text style={{ color: '#f44336' }}>
                            {fieldState.error?.message}
                          </Text>
                        )}
                      </View>
                    )}
                  />
                </View>
                <View style={styles.viewControl}>
                  <Text style={styles.title}>
                    Price <Text style={{ color: 'red' }}>*</Text>:
                  </Text>

                  <Controller
                    name="price"
                    control={control}
                    defaultValue={null}
                    rules={{
                      required: { value: true, message: 'Tiền là bắt buộc' },
                    }}
                    render={({ value, field, fieldState }) => (
                      <View>
                        <TextInput
                          onChangeText={(text) => handleBeforeChange(text, field)}
                          keyboardType={'numeric'}
                          style={[
                            styles.input,
                            {
                              borderColor: 'black',
                            },
                          ]}
                          placeholder="Nhập tiền"
                          placeholderTextColor={'#333'}
                        />
                        <View style={{ flexDirection: 'column' }}>
                          <Dropdown
                            items={currencies}
                            styleCmp={{
                              color: '#000',
                              fontSize: 18,
                              bold: 'bold',
                            }}
                            defaultValue={currency}
                            onValueChange={handleCurrencyChange}></Dropdown>

                          <View>
                            <TextInput
                              onChangeText={text => field.onChange(text)}
                              keyboardType={'numeric'}
                              editable={false}
                              placeholder={'Tiền ELON'}
                              value={field.value}
                              style={[
                                styles.input,
                                {
                                  borderColor: fieldState.invalid
                                    ? '#f44336'
                                    : 'black',
                                  backgroundColor: '#f3f3f3',
                                },
                              ]}
                              placeholderTextColor={'#333'}
                            />
                            <Text>1 ELON = 3300 VND / 0.15USD</Text>
                          </View>
                        </View>
                        {fieldState.invalid && (
                          <Text style={{ color: '#f44336' }}>
                            {fieldState.error?.message}
                          </Text>
                        )}
                      </View>
                    )}
                  />
                </View>
                <View style={styles.viewControl}>
                  <Text style={styles.title}>
                    Hình ảnh <Text style={{ color: 'red' }}>*</Text>:
                  </Text>
                  <Controller
                    name="background"
                    control={control}
                    defaultValue={null}
                    rules={{
                      required: {
                        value: true,
                        message: 'Hình ảnh là bắt buộc',
                      },
                    }}
                    render={({ value, field, fieldState }) => (
                      <View>
                        {!filePath ? (
                          <TouchableOpacity
                            activeOpacity={0.5}
                            style={styles.buttonStyle}
                            onPress={() => selectOneFile('photo', field)}>
                            <Text style={styles.textStyle}>Chọn ảnh</Text>
                          </TouchableOpacity>
                        ) : (
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <Text style={[styles.text, { color: 'black' }]}>
                              {filePath.name
                                .split('.')[0]
                                .slice(0, filePath.name.length / 3)}
                              ... .{filePath.name.split('.')[1]}
                            </Text>
                            <Pressable onPress={() => setFilePath(null)}>
                              <AntDesignIcon
                                name={'close'}
                                color={'black'}
                                size={20}
                              />
                            </Pressable>
                          </View>
                        )}
                        {fieldState.invalid && (
                          <Text style={{ color: '#f44336' }}>
                            {fieldState.error?.message}
                          </Text>
                        )}
                      </View>
                    )}
                  />
                </View>
                <View
                  style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <TouchableOpacity
                    style={styles.btnContainerCancel}
                    onPress={handleBack}>
                    <Text style={(styles.text, { color: 'black' })}>Back</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.btnContainer}
                    onPress={handleSubmit(onSubmit)}>
                    <Text style={styles.text}>Upload</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Animated.ScrollView>
      </SafeAreaView>
    );
  } else return <Loading />;
};

export default CreatePost;
