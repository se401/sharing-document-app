import { useFocusEffect, useTheme } from '@react-navigation/native';
import React, { useCallback, useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Pressable,
  SafeAreaView,
  StyleSheet,
  TextInput,
  View,
} from 'react-native';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import { SharedElement } from 'react-native-shared-element';
import { getAllBook } from '../../../api/services/book.service';
import {
  getActivePosts,
  getPostByAuthorId,
} from '../../../api/services/post.service';
import BookListV2 from '../../../components/Book/BookListV2';
import BookV2 from '../../../components/Book/BookV2';
import Book from '../../../components/Book/SearchBox';
import Text from '../../../components/Text/Text';

const BookSearch = ({ navigation, route: { params } }) => {
  const loaded = useSharedValue(0);
  const [bookList, setBookList] = useState([]);
  const [postList, setPostList] = useState([]);
  const [query, setQuery] = useState(params.searchTerm);
  const [tab, setTab] = useState(0);
  const [loading, setLoading] = useState(false);

  const scrollY = useSharedValue(0);

  const loadData = useCallback(async () => {
    const postRes = (
      await getActivePosts(
        10,
        0,
        'CreatedDate',
        'desc',
        'Title',
        'contains',
        params.searchTerm,
      )
    ).data;

    const posts = postRes.payload;
    console.log('loadData ~ posts', posts);

    setPostList(posts);
    setLoading(false);
  }, []);
  const { colors, height, margin, status } = useTheme();

  const refreshPost = useCallback(async () => {
    const postRes = (
      await getActivePosts(
        10,
        0,
        'CreatedDate',
        'desc',
        'Title',
        'contains',
        query ? query : '',
      )
    ).data;

    setPostList(postRes.payload);
    setLoading(false);
  }, [query]);

  useEffect(() => {
    if (loaded.value === 1) {
      refreshPost();
    }
  }, [query]);

  useEffect(() => {
    if (loaded.value === 1) {
      refreshPost();
    }
  }, [query]);

  useFocusEffect(
    React.useCallback(() => {
      setLoading(true);
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  );

  const anims = {
    search: useAnimatedStyle(() => ({
      zIndex: 10,
      alignItems: 'center',
      flexDirection: 'row',
      paddingTop: status,
      padding: margin / 2,
      justifyContent: 'space-between',
      backgroundColor: colors.background,
      shadowOpacity: interpolate(
        scrollY.value,
        [0, 20],
        [0, 0.75],
        Extrapolate.CLAMP,
      ),
    })),
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
    tabContent: {
      justifyContent: 'center',
      height: '80%',
    },
    sharedElement: {
      flex: 1,
      height: 40,
    },
    searchInput: {
      flex: 1,
      height: 40,
      fontSize: 15,
      borderRadius: 20,
      color: colors.text,
      paddingHorizontal: 20,
      borderWidth: 1,
      borderColor: colors.background,
      backgroundColor: colors.card,
    },
    saveButton: {
      width: 60,
      textAlign: 'right',
      color: '#888888',
    },
    placeholderBox: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#fff',
    },
    placeholderImg: {
      width: '55%',
      height: height / 3,
    },
    placeholderText: {
      marginVertical: margin,
      paddingHorizontal: margin * 3,
    },
    scrollContainer: {
      backgroundColor: '#fff',
    },
    tabContainer: {
      backgroundColor: colors.background,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    divider: {
      borderBottomWidth: 2,
      borderBottomColor: '#CCC',
    },
    active: {
      color: '#bf2e1b',
      borderBottomWidth: 2,
      borderBottomColor: '#bf2e1b',
      padding: 20,
    },
  });

  // Empty screen placeholder
  const PlaceHolder = () => (
    <View style={styles.placeholderBox}>
      <Text center style={styles.placeholderText}>
        You can search by book title, author, keywords etc...
      </Text>
    </View>
  );

  // Return with book list
  const goBack = () => {
    loaded.value = withTiming(0);
    navigation.goBack();
  };

  const scrollHandler = useAnimatedScrollHandler({
    onScroll: ({ contentOffset }) => {
      scrollY.value = contentOffset.y;
    },
  });

  const handleTabChange = (event, tab) => {
    setTab(tab);
  };

  return (
    <View style={styles.container}>
      <Animated.View style={anims.search}>
        <SharedElement style={styles.sharedElement} id="search">
          <TextInput
            autoFocus
            value={query}
            autoCorrect={false}
            onChangeText={text => setQuery(text)}
            placeholder="Find your next book..."
            style={styles.searchInput}
          />
        </SharedElement>
        <Pressable onPress={goBack}>
          <Text bold style={styles.saveButton}>
            Done
          </Text>
        </Pressable>
      </Animated.View>
      <View style={styles.divider} />

      {loading && (
        <View style={styles.tabContent}>
          <ActivityIndicator size="large" color={'red'} />
        </View>
      )}
      {postList && !postList.length && <PlaceHolder />}
      {postList && postList.length > 0 && (
        <SafeAreaView>
          <BookListV2
            books={postList}
            title=""
            navigation={navigation}
            horizontal={false}
          />
        </SafeAreaView>
      )}
    </View>
  );
};

export default BookSearch;
