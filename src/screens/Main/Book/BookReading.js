import React, { useEffect } from 'react';
import { useState } from 'react';
import { viewPost } from '../../../api/services/post.service';
import MyWebView from '../../../components/MyWebview/MyWebView';

const BookReading = ({ route, navigation }) => {
  const [post, setPost] = useState(route.params.post);

  useEffect(() => {
    viewPost(route.params.post.id).then(res =>
      console.log('viewPost', res.data.payload),
    );
  }, []);

  return <MyWebView uri={post.url} />;
};
export default BookReading;
