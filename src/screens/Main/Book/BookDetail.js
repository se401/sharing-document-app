import { useIsFocused, useTheme } from '@react-navigation/native';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { ActivityIndicator, Image, Pressable, StatusBar, View } from 'react-native';
import { PanGestureHandler, ScrollView } from 'react-native-gesture-handler';
import HTMLView from 'react-native-htmlview';
import { Modalize } from 'react-native-modalize';
import Animated, {
  interpolate, runOnJS,
  useAnimatedGestureHandler, useAnimatedScrollHandler, useAnimatedStyle, useSharedValue, withTiming
} from 'react-native-reanimated';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { getBookById } from '../../../api/services/book.service';
import List from '../../../components/Book/BookList';
import Button from '../../../components/Button/Button';
import Text from '../../../components/Text/Text';
import BookHeader from './BookHeader';



const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

const getIcon = (stat) => {
  switch (stat) {
    case 'Reading':
      return 'rocket1';
    case 'Completed':
      return 'Trophy';
    case 'Wishlist':
      return 'book';
    default:
      return 'plus';
  }
};


const BookDetail = ({ route, navigation }) => {
  const [book, setBook] = useState(null);
  const [bookId, setBookId] = useState(route.params.bookId);
  const loaded = useSharedValue(0);
  const [bookList, setBookList] = useState([]);
  const [enabled, setEnabled] = useState(true);
  const panRef = useRef(null);
  const sheetRef = useRef(null);
  const y = useSharedValue(0);
  const closing = useSharedValue(0);
  const scrollY = useSharedValue(0);
  const {
    margin, width, dark, colors, normalize, status,
  } = useTheme();
  const HEADER = normalize(width + status, 500) + margin;

  // Save data to async storage
  // const saveData = async () => {
  //   await AsyncStorage.setItem('@lists', JSON.stringify(bookList));
  // };

  const loadData = useCallback(async () => {
    const bookRes = (await getBookById(route.params.bookId || bookId)).data;
    setBook(bookRes);
    loaded.value = withTiming(1);
  }, [route.params.bookId]);
  // Save data on list change
  useEffect(() => {
    loadData()
  }, [loadData]);

  // Go back to previous screen
  const goBack = () => {
    navigation.goBack();
  };

  // Open book lists sheet
  const openSheet = () => {
    sheetRef.current?.open();
  };

  // Close book list sheet
  const closeSheet = () => {
    sheetRef.current?.close();
  };

  // Add book to list
  const addBook = (list) => {
    // Find book in list and update
    const item = bookList.find((b) => b.id === book.id);
    if (item) {
      setBookList((arr) => {
        arr.splice(bookList.indexOf(item), 1);
        if (list === 'Remove') {
          return [...arr];
        }
        return [{ ...item, status: list }, ...arr];
      });
    } else {
      // Add to list with proper status
      setBookList((arr) => [{ ...book, status: list }, ...arr]);
    }

    closeSheet();
  };

  // Scroll handler
  const scrollHandler = useAnimatedScrollHandler((event) => {
    scrollY.value = event.contentOffset.y;
    if (event.contentOffset.y <= 0 && !enabled) {
      runOnJS(setEnabled)(true);
    }
    if (event.contentOffset.y > 0 && enabled) {
      runOnJS(setEnabled)(false);
    }
  });

  // Pan gesture handler
  const gestureHandler = useAnimatedGestureHandler({
    onStart: (_, ctx) => {
      ctx.startY = y.value;
    },
    onActive: (e, ctx) => {
      const moved = ctx.startY + e.translationY;
      y.value = moved > 0 ? moved : 0;

      // See if closing screen
      if ((y.value >= 75 || e.velocityY >= 500) && !closing.value) {
        closing.value = 1;
        runOnJS(goBack)();
      }
    },
    onEnd: (e) => {
      if (y.value < 75 && e.velocityY < 500) {
        y.value = withTiming(0);
      }
    },
  });


  // Screen anims
  const anims = {
    screen: useAnimatedStyle(() => ({
      transform: [
        { translateY: y.value },
        { scale: interpolate(y.value, [0, 75], [1, 0.90], 'clamp') },
      ],
    })),
    details: useAnimatedStyle(() => ({
      opacity: loaded.value,
      transform: [
        { translateY: interpolate(loaded.value, [0, 1], [20, 0], 'clamp') },
      ],
    })),
  };

  // Styles
  const styles = {
    screen: {
      flex: 1,
      shadowRadius: 10,
      shadowOpacity: 0.5,
      shadowOffset: { height: 5 },
    },

    scrollView: {
      flex: 1,
      borderRadius: 20,
      backgroundColor: colors.background,
    },
    closeIcon: {
      zIndex: 10,
      top: margin,
      right: margin,
      opacity: 0.75,
      color: colors.text,
      position: 'absolute',
    },
    scrollContainer: {
      paddingTop: HEADER,
      paddingBottom: status + 50,
    },
    detailsBox: {
      borderRadius: 10,
      flexDirection: 'row',
      marginHorizontal: margin,
      backgroundColor: colors.card,
    },
    detailsRow: {
      flex: 1,
      paddingVertical: margin / 2,
    },
    detailsRowBorder: {
      borderLeftWidth: 1,
      borderRightWidth: 1,
      borderColor: dark ? '#ffffff22' : '#00000011',
    },
    subDetails: {
      fontSize: 15,
      textAlign: 'center',
      marginTop: margin / 4,
    },
    authorBox: {
      marginTop: margin,
      flexDirection: 'row',
      alignItems: 'center',
      marginHorizontal: margin,
    },
    authorImage: {
      width: 65,
      height: 65,
      borderRadius: 65,
      marginRight: margin,
    },
    authorDetails: {
      marginTop: 5,
      opacity: 0.75,
      width: width - 120,
    },
    aboutBook: {
      margin,
      lineHeight: 25,
      textAlign: 'justify',
    },
    addButton: {
      width: 60,
      height: 60,
      right: margin,
      bottom: margin,
      borderRadius: 60,
      position: 'absolute',
      backgroundColor: colors.button,
    },
    addIcon: {
      top: 3,
    },
    modal: {
      padding: margin,
      borderRadius: 12,
      paddingBottom: status,
      backgroundColor: colors.card,
    },
    flexRow: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    marginB: {
      marginBottom: margin,
    },
    readNow: {
      marginTop: margin,
      backgroundColor: 'red',
      color: '#fff'
    },
    iconBtn: {
      padding: 0,
      backgroundColor: colors.card,
    },
    iconLeft: {
      fontSize: 21,
      color: colors.text,
      marginRight: margin,
    },
    middle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    }
  };


  if (!book) {
    return (
      <View style={styles.middle}>
        <ActivityIndicator size='large' color={'red'} />
      </View>
    )
  } else {
    return (
      <>
        <PanGestureHandler
          ref={panRef}
          failOffsetY={-5}
          activeOffsetY={5}
          enabled={enabled}
          onGestureEvent={gestureHandler}
        >
          <Animated.View style={anims.screen}>
            <StatusBar hidden={true} animated />
            <BookHeader scrollY={scrollY} y={y} book={book} navigation={navigation} />
            <AntDesign size={27} name="close" onPress={goBack} style={styles.closeIcon} />

            <Animated.View style={anims.scrollView}>
              <AnimatedScrollView
                waitFor={enabled ? panRef : undefined}
                onScroll={scrollHandler}
                scrollEventThrottle={8}
                contentContainerStyle={styles.scrollContainer}
              >
                <View style={styles.detailsBox}>
                  <View style={styles.detailsRow}>
                    <Text center size={13}>RATING</Text>
                    <Text bold style={styles.subDetails}>{book.volumeInfo.averageRating}</Text>
                  </View>
                  <View style={[styles.detailsRow, styles.detailsRowBorder]}>
                    <Text center size={13}>PAGES</Text>
                    <Text bold style={styles.subDetails}>{book.volumeInfo.pageCount}</Text>
                  </View>
                  <Pressable onPress={openSheet} style={styles.detailsRow}>
                    <Text center size={13}>PUSHLISHED DATE</Text>
                    <Text bold color={colors.primary} style={styles.subDetails}>{book.volumeInfo.publishedDate}</Text>
                  </Pressable>
                </View>

                <View style={[styles.detailsBox, styles.readNow]}>
                  <Pressable onPress={openSheet} style={styles.detailsRow}>
                    <Text center size={13} style={{color: '#fff'}}>READ NOW</Text>
                  </Pressable>
                </View>

                <Animated.View style={anims.details}>
                  {/* <View style={styles.authorBox}>
                    <Image source={{ uri: author?.image_url }} style={styles.authorImage} />
                    <View>
                      <Text bold size={17} style={{color: '#f2f2f2'}}>Author: {book?.volumeInfo.authors[0] || '...'}</Text>
                      <Text numberOfLines={2} style={styles.authorDetails}>
                      </Text>
                    </View>
                  </View> */}
                  {/* <HTMLView value={book?.volumeInfo.description || '...'} stylesheet={styles.aboutBook}>

                  </HTMLView> */}
                  <Text size={16} style={styles.aboutBook}>
                    {book.volumeInfo.description.replace(/(<([^>]+)>)/ig, '\u000a')}
                  </Text>
                  {/* <List books={related} title="Related Books" navigation={navigation} /> */}
                </Animated.View>
              </AnimatedScrollView>

              <Button onPress={openSheet} style={styles.addButton}>
                <AntDesign size={21} name={'rocket1'} style={styles.addIcon} />
              </Button>
            </Animated.View>
          </Animated.View>
        </PanGestureHandler>

        <Modalize ref={sheetRef} threshold={50} adjustToContentHeight>
          <View style={styles.modal}>
            <View style={[styles.flexRow, styles.marginB]}>
              <Text bold size={20}>Add To</Text>
              <Text bold onPress={closeSheet}>Done</Text>
            </View>
            <Pressable onPress={() => addBook('Reading')} style={[styles.flexRow, styles.marginB]}>
              <AntDesign.Button onPress={() => addBook('Reading')} name="rocket1" style={styles.iconBtn} iconStyle={styles.iconLeft}>
                <Text size={17}>Reading</Text>
              </AntDesign.Button>
              <AntDesign size={21} color={colors.text} name={'check'} />
            </Pressable>
            <Pressable onPress={() => addBook('Completed')} style={[styles.flexRow, styles.marginB]}>
              <AntDesign.Button onPress={() => addBook('Completed')} name="Trophy" style={styles.iconBtn} iconStyle={styles.iconLeft}>
                <Text size={17}>Completed</Text>
              </AntDesign.Button>
              <AntDesign size={21} color={colors.text} name={'user'} />
            </Pressable>
            <Pressable onPress={() => addBook('Wishlist')} style={[styles.flexRow, styles.marginB]}>
              <AntDesign.Button onPress={() => addBook('Wishlist')} name="book" style={styles.iconBtn} iconStyle={styles.iconLeft}>
                <Text size={17}>Wishlist</Text>
              </AntDesign.Button>
              <AntDesign size={21} color={colors.text} name={'check'} />
            </Pressable>
            <Pressable onPress={() => addBook('Remove')}>
              <Text center size={16} color="#ff3b30">Remove</Text>
            </Pressable>
          </View>
        </Modalize>
      </>
    );
  }


}
export default React.memo(BookDetail);