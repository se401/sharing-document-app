import { useTheme } from '@react-navigation/native';
import { format } from 'date-fns';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
  ActivityIndicator,
  Alert,
  Pressable,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { ListItem } from 'react-native-elements';
import RNFS from 'react-native-fs';
import {
  PanGestureHandler,
  ScrollView,
  TextInput,
  TouchableOpacity as TouchableOpacityGesture,
} from 'react-native-gesture-handler';
import { Modalize } from 'react-native-modalize';
import Animated, {
  interpolate,
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontawesomeIcons from 'react-native-vector-icons/FontAwesome';
import { saveBookMart } from '../../../api/services/account.service';
import Share from 'react-native-share';
import {
  createComment,
  getCommentsByPost,
} from '../../../api/services/comment.service';
import {
  buyPost,
  getPostByCateId,
  getPostById,
  getUrl,
  likePost,
  viewPost,
} from '../../../api/services/post.service';
import BookListV2 from '../../../components/Book/BookListV2';
import Message from '../../../components/Messages/Message';
import Text from '../../../components/Text/Text';
import BookHeaderV2 from './BookHeaderV2';

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

const getIcon = stat => {
  switch (stat) {
    case 'Reading':
      return 'rocket1';
    case 'Completed':
      return 'Trophy';
    case 'Wishlist':
      return 'book';
    default:
      return 'plus';
  }
};

const BookDetail = ({ route, navigation }) => {
  const [post, setPost] = useState(null);
  const [postId, setPostId] = useState(route.params.postId);
  const [isPaid, setIsPaid] = useState('');
  const loaded = useSharedValue(0);
  const [bookList, setBookList] = useState([]);
  const [related, setRelated] = useState([]);

  const [enabled, setEnabled] = useState(true);
  const panRef = useRef(null);
  const sheetRef = useRef(null);

  const commentRef = useRef(null);
  const [comments, setComments] = useState([]);
  const [commenting, setCommenting] = useState(false);
  const commentController = useRef(null);

  const [reply, setReply] = useState(null);
  const replyRef = useRef(null);

  const y = useSharedValue(0);
  const closing = useSharedValue(0);
  const scrollY = useSharedValue(0);
  const { margin, width, dark, colors, normalize, status } = useTheme();
  const HEADER = normalize(width + status, 500) + margin;

  const [loading, setLoading] = useState(false);

  const url =
    'https://github.com/vinzscam/react-native-file-viewer/raw/master/docs/react-native-file-viewer-certificate.pdf';
  const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.pdf`;

  // Save data to async storage
  // const saveData = async () => {
  //   await AsyncStorage.setItem('@lists', JSON.stringify(bookList));
  // };

  const loadData = useCallback(async () => {
    const postRes = (await getPostById(route.params.bookId || postId)).data;
    setPost(postRes.payload);

    const commentRes = (await getCommentsByPost(route.params.bookId || postId))
      .data;
    setComments(commentRes.payload);

    const postRelatedListRes = (
      await getPostByCateId(postRes.payload.category.id, 10, 1)
    ).data;

    const isPaidRes = (await getUrl(postRes.payload.id)).data;
    setIsPaid(isPaidRes.payload);

    const posts = postRelatedListRes.payload.filter(post => post.id !== postId);

    setRelated(posts);
    loaded.value = withTiming(1);
  }, [route.params.postId]);
  // Save data on list change
  useEffect(() => {
    loadData();
  }, [loadData, loading, commenting]);

  // Go back to previous screen
  const goBack = () => {
    navigation.goBack();
  };

  // Open book lists sheet
  const openSheet = () => {
    sheetRef.current?.open();
  };

  const openCommentSheet = () => {
    commentRef.current?.open();
    closeSheet();
  };

  const openReplySheet = () => {
    replyRef.current?.open();
    closeCommentSheet();
  };

  // Close book list sheet
  const closeSheet = () => {
    sheetRef.current?.close();
  };

  const closeCommentSheet = () => {
    commentRef.current?.close();
  };

  const closeReplySheet = () => {
    replyRef.current?.close();
  };

  const openShare = () => {
    const shareOptions = {
      title: post.title,
      message: post.title + '/n' + post.description,
      url: post.url,
      showAppsToView: true,
      // social: Share.Social.FACEBOOK
    };
    console.log('openShare ~ shareOptions', shareOptions);
    Share.open(shareOptions)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        err && console.log(err);
      });
  };

  // Add book to list
  const addBook = list => {
    // Find book in list and update
    const item = bookList.find(b => b.id === post.id);
    if (item) {
      setBookList(arr => {
        arr.splice(bookList.indexOf(item), 1);
        if (list === 'Remove') {
          return [...arr];
        }
        return [{ ...item, status: list }, ...arr];
      });
    } else {
      // Add to list with proper status
      setBookList(arr => [{ ...post, status: list }, ...arr]);
    }

    closeSheet();
  };

  const ratePost = () => {
    likePost(postId, true).then(async res => {
      if (res.data.payload) {
        await loadData();
        closeSheet();
      }
    });
  };

  const gotoDetailRead = async () => {
    if (post.price === 0 || isPaid) {
      console.log('gotoDetailRead ~ isPaid', isPaid);
      const updatedPost = { ...post, url: isPaid };
      navigation.navigate('BookReading', { post: updatedPost });
      return;
    }
    if (post.price > 0) {
      if (!isPaid) {
        Alert.alert(
          'Thông báo',
          'Xác nhận thanh toán để xem tài liệu!!!',
          [
            {
              text: 'Hủy',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: 'Xác nhận',
              onPress: () => {
                setLoading(true);
                buyPost(postId).then(res => {
                  if (res.data.statusCode === 200) {
                    getUrl(postId).then(res => {
                      if (res.data.statusCode === 200) {
                        setLoading(false);
                        const updatedPost = { ...post, url: res.data.payload };
                        navigation.navigate('BookReading', {
                          post: updatedPost,
                        });
                      }
                    });
                  }
                });
              },
            },
          ],
          {
            cancelable: true,
          },
        );
      }
    }
  };

  // Scroll handler
  const scrollHandler = useAnimatedScrollHandler(event => {
    scrollY.value = event.contentOffset.y;
    if (event.contentOffset.y <= 0 && !enabled) {
      runOnJS(setEnabled)(true);
    }
    if (event.contentOffset.y > 0 && enabled) {
      runOnJS(setEnabled)(false);
    }
  });

  // Pan gesture handler
  const gestureHandler = useAnimatedGestureHandler({
    onStart: (_, ctx) => {
      ctx.startY = y.value;
    },
    onActive: (e, ctx) => {
      const moved = ctx.startY + e.translationY;
      y.value = moved > 0 ? moved : 0;

      // See if closing screen
      if ((y.value >= 75 || e.velocityY >= 500) && !closing.value) {
        closing.value = 1;
        runOnJS(goBack)();
      }
    },
    onEnd: e => {
      if (y.value < 75 && e.velocityY < 500) {
        y.value = withTiming(0);
      }
    },
  });

  // Screen anims
  const anims = {
    screen: useAnimatedStyle(() => ({
      transform: [
        { translateY: y.value },
        { scale: interpolate(y.value, [0, 75], [1, 0.9], 'clamp') },
      ],
    })),
    details: useAnimatedStyle(() => ({
      opacity: loaded.value,
      transform: [
        { translateY: interpolate(loaded.value, [0, 1], [20, 0], 'clamp') },
      ],
    })),
  };

  // Styles
  const styles = StyleSheet.create({
    screen: {
      flex: 1,
      shadowRadius: 10,
      shadowOpacity: 0.5,
      shadowOffset: { height: 5 },
    },
    scrollView: {
      flex: 1,
      borderRadius: 20,
      backgroundColor: colors.background,
    },
    closeIcon: {
      zIndex: 10,
      top: margin,
      right: margin,
      opacity: 0.75,
      color: colors.text,
      position: 'absolute',
    },
    scrollContainer: {
      paddingTop: HEADER,
      paddingBottom: status + 50,
    },
    detailsBox: {
      borderRadius: 10,
      flexDirection: 'row',
      marginHorizontal: margin,
      backgroundColor: colors.card,
    },
    detailsRow: {
      flex: 1,
      paddingVertical: margin / 2,
    },
    detailsRowBorder: {
      borderLeftWidth: 1,
      borderRightWidth: 1,
      borderColor: dark ? '#ffffff22' : '#00000011',
    },
    subDetails: {
      fontSize: 15,
      textAlign: 'center',
      marginTop: margin / 4,
    },
    authorBox: {
      marginTop: margin,
      flexDirection: 'row',
      alignItems: 'center',
      marginHorizontal: margin,
    },
    authorImage: {
      width: 65,
      height: 65,
      borderRadius: 65,
      marginRight: margin,
    },
    authorDetails: {
      marginTop: 5,
      opacity: 0.75,
      width: width - 120,
    },
    aboutBook: {
      margin,
      lineHeight: 25,
      textAlign: 'justify',
      color: '#010101',
    },
    addButton: {
      width: 60,
      height: 60,
      right: margin,
      bottom: margin,
      borderRadius: 60,
      position: 'absolute',
      zIndex: 100,
      backgroundColor: colors.button,
    },
    addIcon: {
      top: '30%',
      left: '30%',
    },
    modal: {
      paddingLeft: margin,
      paddingRight: margin,
      paddingTop: margin,
      borderRadius: 12,
      paddingBottom: status,
      backgroundColor: colors.card,
    },
    flexRow: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    marginB: {
      marginBottom: margin,
    },
    readNow: {
      marginTop: margin,
      backgroundColor: 'red',
      color: '#fff',
    },
    iconBtn: {
      padding: 5,
      backgroundColor: colors.card,
    },
    iconLeft: {
      fontSize: 21,
      color: colors.text,
    },
    middle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      top: '50%',
      left: '50%',
    },
    shortInfo: {
      borderRadius: 10,
      marginTop: 10,
      backgroundColor: colors.card,
      marginHorizontal: margin,
      position: 'relative',
      zIndex: 1000,
    },
    commentHeader: {
      paddingTop: margin,
      paddingRight: margin,
      paddingLeft: margin,
      paddingBottom: margin / 2,
    },
  });

  const handleReply = cmtId => {
    let replyObj = comments.find(cmt => cmt.id === cmtId);
    setReply(replyObj);
    openReplySheet();
  };

  const saveBookMark = () => {
    saveBookMart(route.params.bookId || postId).then(res => {
      if (res.data.statusCode === 200) {
        Alert.alert(
          'Thành công',
          'Quan tâm tài liệu thành công.',
          [
            {
              text: 'Hủy',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: 'Xác nhận',
              onPress: () => {
                closeSheet();
                closeCommentSheet();
                closeReplySheet();
              },
            },
          ],
          {
            cancelable: true,
          },
        );
      }
    });
  };

  const onSubmit = e => {
    setCommenting(true);

    createComment(e.nativeEvent.text, reply ? reply.id : '', route.params.postId)
      .then(res => {
        if (res.data.statusCode === 200) {
          setCommenting(false);
          setReply(null);
          commentController.current?.clear();
        }
      })
      .catch(e => setCommenting(false));
  };

  if (!post) {
    return (
      <View style={styles.middle}>
        <ActivityIndicator size="large" color={'red'} />
      </View>
    );
  } else {
    return (
      <>
        <PanGestureHandler
          ref={panRef}
          failOffsetY={-5}
          activeOffsetY={5}
          enabled={enabled}
          onGestureEvent={gestureHandler}>
          <Animated.View style={anims.screen}>
            <StatusBar hidden={true} animated />
            <BookHeaderV2
              scrollY={scrollY}
              y={y}
              book={post}
              navigation={navigation}
            />
            <AntDesign
              size={27}
              name="close"
              onPress={goBack}
              style={styles.closeIcon}
            />

            <Animated.View style={anims.scrollView}>
              <AnimatedScrollView
                waitFor={enabled ? panRef : undefined}
                onScroll={scrollHandler}
                scrollEventThrottle={8}
                contentContainerStyle={styles.scrollContainer}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                  }}>
                  <TouchableOpacityGesture
                    onPress={() => saveBookMark()}
                    style={[styles.flexRow, styles.marginB]}>
                    <AntDesign.Button
                      name="rocket1"
                      style={[styles.iconBtn]}
                      iconStyle={styles.iconLeft}>
                      <Text size={14}>Quan tâm</Text>
                    </AntDesign.Button>
                  </TouchableOpacityGesture>
                  <TouchableOpacityGesture
                    onPress={() => ratePost()}
                    style={[styles.flexRow, styles.marginB]}>
                    <AntDesign.Button
                      name="Trophy"
                      style={[styles.iconBtn]}
                      iconStyle={styles.iconLeft}>
                      <Text size={14}>Thích</Text>
                    </AntDesign.Button>
                  </TouchableOpacityGesture>
                  <TouchableOpacityGesture
                    onPress={() => openCommentSheet()}
                    style={[styles.flexRow, styles.marginB]}>
                    <AntDesign.Button
                      name="book"
                      style={[styles.iconBtn]}
                      iconStyle={styles.iconLeft}>
                      <Text size={14}>Bình luận</Text>
                    </AntDesign.Button>
                  </TouchableOpacityGesture>
                  <TouchableOpacityGesture
                    onPress={() => openShare()}
                    style={[styles.flexRow, styles.marginB]}>
                    <FontawesomeIcons.Button
                      name="share"
                      style={[styles.iconBtn]}
                      iconStyle={styles.iconLeft}>
                      <Text size={14}>Chia sẻ</Text>
                    </FontawesomeIcons.Button>
                  </TouchableOpacityGesture>
                </View>
                <View style={styles.detailsBox}>
                  <View style={styles.detailsRow}>
                    <Text center size={13}>
                      LƯỢT THÍCH
                    </Text>
                    <Text bold style={styles.subDetails}>
                      {post.likeCount}
                    </Text>
                  </View>
                  <View style={[styles.detailsRow, styles.detailsRowBorder]}>
                    <Text center size={13}>
                      LƯỢT XEM
                    </Text>
                    <Text bold style={styles.subDetails}>
                      {post.viewCount}
                    </Text>
                  </View>
                  <Pressable onPress={openSheet} style={styles.detailsRow}>
                    <Text center size={13}>
                      NGÀY ĐĂNG
                    </Text>
                    <Text bold color={colors.primary} style={styles.subDetails}>
                      {format(new Date(post.createdDate), 'MM/dd/yyyy')}
                    </Text>
                  </Pressable>
                </View>

                <View style={[styles.detailsBox, styles.readNow]}>
                  <Pressable onPress={gotoDetailRead} style={styles.detailsRow}>
                    <Text center size={18} style={{ color: '#fff' }}>
                      XEM NGAY{'  '}
                      {post.price === 0 ? null : (
                        <FontawesomeIcons
                          name={isPaid ? 'unlock' : 'lock'}
                          size={20}
                          style={{ paddingLeft: 20 }}
                          color={'#fff'}></FontawesomeIcons>
                      )}
                    </Text>
                  </Pressable>
                </View>

                <Animated.View style={styles.shortInfo}>
                  <ListItem>
                    <ListItem.Content>
                      <ListItem.Title>
                        <Text>* Thể loại: {post.category?.name}</Text>
                      </ListItem.Title>
                    </ListItem.Content>
                  </ListItem>
                  <ListItem>
                    <ListItem.Content>
                      <ListItem.Title>
                        <Text>* Mức độ: {post.level?.name}</Text>
                      </ListItem.Title>
                    </ListItem.Content>
                  </ListItem>
                  <ListItem>
                    <ListItem.Content>
                      <ListItem.Title>
                        <Text>* Nguồn: {post.resource?.name}</Text>
                      </ListItem.Title>
                    </ListItem.Content>
                  </ListItem>
                </Animated.View>

                <Animated.View style={[anims.details, styles.shortInfo]}>
                  {/* <View style={styles.authorBox}>
                    <Image source={{ uri: author?.image_url }} style={styles.authorImage} />
                    <View>
                      <Text bold size={17} style={{color: '#f2f2f2'}}>Author: {book?.volumeInfo.authors[0] || '...'}</Text>
                      <Text numberOfLines={2} style={styles.authorDetails}>
                      </Text>
                    </View>
                  </View> */}
                  {/* <HTMLView value={book?.volumeInfo.description || '...'} stylesheet={styles.aboutBook}>

                  </HTMLView> */}
                  <Text size={16} style={styles.aboutBook}>
                    {post.description.replace(/(<([^>]+)>)/gi, '\u000a')}
                  </Text>
                </Animated.View>
                <Animated.View style={[styles.shortInfo]}>
                  <BookListV2
                    books={related}
                    title="Liên quan"
                    navigation={navigation}
                  />
                </Animated.View>
              </AnimatedScrollView>

              <TouchableOpacity onPress={openSheet} style={styles.addButton}>
                <AntDesign size={21} name={'rocket1'} style={styles.addIcon} />
              </TouchableOpacity>
            </Animated.View>
          </Animated.View>
        </PanGestureHandler>

        {loading && (
          <View style={styles.middle}>
            <ActivityIndicator size="large" color={'red'} />
          </View>
        )}

        <Modalize ref={sheetRef} threshold={50} adjustToContentHeight>
          <View style={styles.modal}>
            <View style={[styles.flexRow, styles.marginB]}>
              <Text bold size={20}>
                Thêm vào
              </Text>
              <Text bold onPress={closeSheet}>
                Done
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => saveBookMark()}
              style={[styles.flexRow, styles.marginB]}>
              <AntDesign.Button
                name="rocket1"
                style={styles.iconBtn}
                iconStyle={styles.iconLeft}>
                <Text size={17}>Quan tâm</Text>
              </AntDesign.Button>
              <AntDesign size={21} color={colors.text} name={'check'} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => ratePost()}
              style={[styles.flexRow, styles.marginB]}>
              <AntDesign.Button
                name="Trophy"
                style={styles.iconBtn}
                iconStyle={styles.iconLeft}>
                <Text size={17}>Thích</Text>
              </AntDesign.Button>
              <AntDesign size={21} color={colors.text} name={'user'} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => openCommentSheet()}
              style={[styles.flexRow, styles.marginB]}>
              <AntDesign.Button
                name="book"
                style={styles.iconBtn}
                iconStyle={styles.iconLeft}>
                <Text size={17}>Bình luận</Text>
              </AntDesign.Button>
              <AntDesign size={21} color={colors.text} name={'check'} />
            </TouchableOpacity>
          </View>
        </Modalize>
        <Modalize
          ref={commentRef}
          withHandle={false}
          adjustToContentHeight
          HeaderComponent={
            <>
              <View style={[styles.flexRow, styles.commentHeader]}>
                <Text bold size={20}>
                  Bình luận
                </Text>
                <Text size={20} bold onPress={closeCommentSheet}>
                  X
                </Text>
              </View>
            </>
          }
          FooterComponent={
            <>
              <TextInput
                ref={commentController}
                placeholder={'Bình luận'}
                placeholderTextColor={'#fff'}
                style={{ backgroundColor: '#333', color: '#fff' }}
                onSubmitEditing={onSubmit}
                clearButtonMode={'always'}></TextInput>
            </>
          }>
          <View style={[styles.modal]}>
            {commenting && (
              <View
                style={{
                  position: 'absolute',
                  left: '50%',
                  top: '50%',
                  zIndex: 1000,
                }}>
                <ActivityIndicator size="large" color={'red'} />
              </View>
            )}
            {!commenting &&
              comments.map(cmt => (
                <Message
                  key={cmt.id}
                  id={cmt.id}
                  replyId={cmt.replyTo?.id}
                  replyText={cmt.replyTo?.text}
                  ownerName={cmt.user?.username}
                  onReply={handleReply}
                  message={cmt.text}
                />
              ))}
            {!commenting && comments.length === 0 && (
              <Text>Không có bình luận</Text>
            )}
          </View>
        </Modalize>

        <Modalize
          ref={replyRef}
          withHandle={false}
          adjustToContentHeight
          HeaderComponent={
            <>
              <View style={[styles.flexRow, styles.commentHeader]}>
                <Text bold size={20}>
                  Trả lời
                </Text>
                <Text size={20} bold onPress={closeCommentSheet}>
                  X
                </Text>
              </View>
            </>
          }
          FooterComponent={
            <>
              <TextInput
                ref={commentController}
                placeholder={'Trả lời'}
                placeholderTextColor={'#fff'}
                style={{ backgroundColor: '#333', color: '#fff' }}
                onSubmitEditing={onSubmit}
                clearButtonMode={'always'}></TextInput>
            </>
          }>
          <View style={[styles.modal]}>
            {commenting && (
              <View
                style={{
                  position: 'absolute',
                  left: '50%',
                  top: '50%',
                  zIndex: 1000,
                }}>
                <ActivityIndicator size="large" color={'red'} />
              </View>
            )}
            {reply && (
              <Message
                id={reply.id}
                ownerName={reply.user?.username}
                onReply={handleReply}
                message={reply.text}
              />
            )}
          </View>
        </Modalize>
      </>
    );
  }
};
export default React.memo(BookDetail);
