import { useFocusEffect, useTheme } from '@react-navigation/native';
import React, { useCallback, useContext, useState } from 'react';
import { Pressable, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Avatar, ListItem } from 'react-native-elements';
import Animated, {
  useAnimatedScrollHandler,
  useSharedValue,
} from 'react-native-reanimated';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {
  getBookmarkPost,
  getLikedPost,
} from '../../../api/services/post.service';
import Text from '../../../components/Text/Text';
import { AppContext } from '../../../context/AppContext';
import NotLogin from './NotLogin';

const ActivityTab = ({ navigation }) => {
  const loaded = useSharedValue(0);
  const [bookmarks, setBookmarks] = useState([]);
  const [liked, setLiked] = useState([]);
  const [loading, setLoading] = useState(false);
  const { width, margin, colors } = useTheme();

  const context = useContext(AppContext);
  const scrollY = useSharedValue(0);

  const scrollHandler = useAnimatedScrollHandler({
    onScroll: ({ contentOffset }) => {
      scrollY.value = contentOffset.y;
    },
  });

  const loadData = useCallback(async () => {
    const bookmarkRes = (await getBookmarkPost(50, 0)).data;
    const likedRes = (await getLikedPost(50, 0)).data;

    setBookmarks(bookmarkRes.payload);
    setLiked(likedRes.payload);
    setLoading(false);
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      setLoading(true);
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  );

  const styles = StyleSheet.create({
    list: {
      backgroundColor: colors.card,
      paddingTop: margin,
      margin: margin,
      borderRadius: 20,
    },
    listContainer: {
      padding: margin,
    },
    heading: {
      paddingTop: margin,
      paddingHorizontal: margin,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    emptyContainer: {
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      width: width - margin * 2,
      paddingVertical: margin * 2,
    },
    emptyText: {
      padding: margin,
    },
  });

  const bookDetails = id => {
    navigation.navigate('BookDetailV2', { postId: id });
  };

  const EmptyList = () => (
    <Pressable style={styles.emptyContainer}>
      <AntDesign color={colors.text} size={27} name="book" />
      <Text size={16} center style={styles.emptyText}>
        Không có bài đăng nào.
      </Text>
    </Pressable>
  );

  const BlockBookmarks = () => (
    <>
      <View style={styles.heading}>
        <Text size={17} bold>
          Đánh dấu
        </Text>
      </View>
      <Animated.ScrollView>
        <View>
          {bookmarks.length > 0 ? (
            bookmarks.map(post => (
              <TouchableOpacity onPress={() => bookDetails(post.id)}>
                <ListItem key={post.id} bottomDivider>
                  <Avatar
                    source={{
                      uri: post.backgroudUrl,
                    }}
                  />
                  <ListItem.Content>
                    <ListItem.Title>{post.title}</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              </TouchableOpacity>
            ))
          ) : (
            <EmptyList />
          )}
        </View>
      </Animated.ScrollView>
    </>
  );

  const BlockLikedPost = () => (
    <>
      <View style={styles.heading}>
        <Text size={17} bold>
          Đã thích
        </Text>
      </View>
      <Animated.ScrollView scrollEventThrottle={8}>
        <View>
          {liked.length > 0 ? (
            liked.map(post => (
              <TouchableOpacity onPress={() => bookDetails(post.id)}>
                <ListItem key={post.id} bottomDivider>
                  <Avatar
                    source={{
                      uri: post.backgroudUrl,
                    }}
                  />
                  <ListItem.Content>
                    <ListItem.Title>{post.title}</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              </TouchableOpacity>
            ))
          ) : (
            <EmptyList />
          )}
        </View>
      </Animated.ScrollView>
    </>
  );

  return (
    <View style={styles.list}>
      {!context.isLogin && <NotLogin navigation={navigation} />}
      {context.isLogin && (
        <>
          <BlockBookmarks />
          <BlockLikedPost />
        </>
      )}
    </View>
  );
};

export default ActivityTab;
