import React, { useState } from 'react';
import { View } from 'react-native';
import { Tab } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import ActivityTab from './ActivityTab';
import MyTab from './MyTab';
import RateTab from './RateTab';

const Activity = ({ navigation }) => {
  const [activeTab, setActiveTab] = useState(0);

  const changeTab = value => {
    setActiveTab(value);
  };

  const handleShowTab = () => {
    switch (activeTab) {
      // case 0:
      //   return <RateTab navigation={navigation} />
      case 1:
        return <ActivityTab navigation={navigation} />;
      default:
        return <MyTab navigation={navigation} />;
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <Tab onChange={changeTab} value={activeTab}>
        {/* <Tab.Item title="Bình luận" titleStyle={{ fontSize: 13, color: activeTab === 0 ? '#FF6347' : '#000' }} containerStyle={{ backgroundColor: 'white' }} /> */}
        <Tab.Item
          title="Của Tôi"
          titleStyle={{
            fontSize: 13,
            color: activeTab === 0 ? '#FF6347' : '#000',
          }}
          containerStyle={{ backgroundColor: 'white' }}
        />
        <Tab.Item
          title="Hoạt động"
          titleStyle={{
            fontSize: 13,
            color: activeTab === 1 ? '#FF6347' : '#000',
          }}
          containerStyle={{ backgroundColor: 'white' }}
        />
      </Tab>
      <SafeAreaView style={{ flex: 1 }}>{handleShowTab()}</SafeAreaView>
    </View>
  );
};

export default Activity;
