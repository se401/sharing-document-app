import { useTheme } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Button from '../../../components/Button/Button';

const NotLogin = ({ navigation }) => {

  const {
    dark, width, height, colors, margin, navbar, normalize,
  } = useTheme();

  const styles = StyleSheet.create({
    notLogin: {
      height: height * 0.7,
      width,
      justifyContent: 'center'
    },
    message: {
      color: colors.text,
      marginBottom: margin,
      fontSize: 16
    },
    notLoginBlock: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    btnLogin: {
      backgroundColor: '#fff',
      borderWidth: 2,
      borderColor: '#dd00dd',
      width: width - 60
    }
  })

  const gotoLogin = () => navigation.navigate('Login')


  return (
    <View style={styles.notLogin}>
      <View style={styles.notLoginBlock}>
        <Text style={styles.message}>
          Bạn cần đăng nhập để theo dõi nhật ký hoạt động
        </Text>
        <Button style={styles.btnLogin} onPress={gotoLogin}>
          Đăng nhập
        </Button>
      </View>
    </View>
  )
}

export default NotLogin;