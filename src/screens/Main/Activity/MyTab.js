import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect, useTheme } from '@react-navigation/native';
import React, { useCallback, useContext, useState } from 'react';
import { ActivityIndicator, FlatList, Pressable, StyleSheet, View } from 'react-native';
import Animated, {
  useAnimatedScrollHandler,
  useSharedValue,
} from 'react-native-reanimated';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { getPostByAuthorId } from '../../../api/services/post.service';
import BookListV2 from '../../../components/Book/BookListV2';
import BookV2 from '../../../components/Book/BookV2';
import Text from '../../../components/Text/Text';
import { AppContext } from '../../../context/AppContext';
import NotLogin from './NotLogin';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const MyTab = ({ navigation }) => {
  const loaded = useSharedValue(0);
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const scrollX = useSharedValue(0);
  const { width, margin, colors } = useTheme();

  const context = useContext(AppContext);

  const loadData = useCallback(async () => {
    const userJson = await AsyncStorage.getItem('@user');
    if (userJson) {
      let user = JSON.parse(userJson);
      const postRes = (await getPostByAuthorId(user.id, 50, 1)).data;

      setPosts(postRes.payload);
    }

    setLoading(false);
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      setLoading(true);
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  );

  const scrollHandler = useAnimatedScrollHandler({
    onScroll: ({ contentOffset }) => {
      scrollX.value = contentOffset.x;
    },
  });

  const styles = StyleSheet.create({
    list: {
      backgroundColor: colors.card,
      paddingTop: margin,
    },
    listContainer: {
      padding: margin,
    },
    heading: {
      paddingTop: margin,
      paddingHorizontal: margin,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    emptyContainer: {
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      width: width - margin * 2,
      paddingVertical: margin * 2,
      backgroundColor: colors.background,
    },
    emptyText: {
      padding: margin,
    },
  });

  const EmptyList = () => (
    <Pressable style={styles.emptyContainer}>
      <AntDesign color={colors.text} size={27} name="book" />
      <Text size={16} center style={styles.emptyText}>
        {'Không có bài đăng nào.'}
      </Text>
    </Pressable>
  );

  return (
    <View style={styles.list}>
      {!context.isLogin && <NotLogin navigation={navigation} />}
      {context.isLogin &&
        (!loading ? (
          <BookListV2
            books={posts}
            title=""
            navigation={navigation}
            horizontal={false}
          />
        ) : (
          <View style={styles.tabContent}>
            <ActivityIndicator size="large" color={'red'} />
          </View>
        ))}
    </View>
  );
};
export default MyTab;
