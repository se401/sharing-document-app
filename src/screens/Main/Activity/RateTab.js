import React from 'react';
import { Linking, TouchableOpacity } from 'react-native';
import { StyleSheet } from 'react-native';
import { View } from 'react-native';
import { Avatar } from 'react-native-elements';
import Text from '../../../components/Text/Text';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';


const RateTab = ({ navigation }) => {
  return (
    <View style={{ marginTop: 10 }}>
      <RateTabItem />
    </View>
  )
}

const RateTabItem = () => {
  return (
    <View style={{ backgroundColor: 'white', }}>
      <View style={styles.rateItemTopContainer}>
        <View style={styles.rateItemTopAvatar}>
          <Avatar
            rounded
            size={'medium'}
            source={{
              uri: 'https://ui-avatars.com/api/?rounded=true&name=' + 'Duc',
            }}
          />
        </View>
        <View style={{ width: '85%' }}>
          <View>
            <Text bold size={20}>Duc</Text>
            <Text>3 minutes ago</Text>
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.wrapText}>
              Sách hay và có thể bắt đầu thực nghiệm để kiểm tra
            </Text>
            <Text style={[styles.link, styles.wrapText]} onPress={() => Linking.openURL('http://google.com')}>
              http://google.com
            </Text>
          </View>
          <View style={styles.rateItemAction}>
            <View style={{ flexDirection: 'row', width: '100%', }}>
              <TouchableOpacity style={{ marginRight: 5 }}>
                <View>
                  <Text>
                    0 lượt xem
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ marginRight: 5 }}>
                <View>
                  <Text>
                    - 0 thích
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Text>
                    - 0 bình luận
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  link: {
    color: 'blue', fontStyle: 'italic', textDecorationLine: 'underline'
  },
  rateItemTopContainer: { padding: 10, flexDirection: 'row' },
  rateItemTopAvatar: { width: '15%', marginRight: 10 },
  wrapText: { flexWrap: 'wrap', width: '100%' },
  rateItemAction: { width: '100%', marginTop: 10 }
})

export default RateTab;