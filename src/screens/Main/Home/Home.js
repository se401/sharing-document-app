import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect, useTheme } from '@react-navigation/native';
import LottieView from 'lottie-react-native';
import React, { useContext, useEffect, useState } from 'react';
import { ActivityIndicator, Pressable, StyleSheet, View } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import Animated, {
  interpolate,
  useAnimatedProps,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { SharedElement } from 'react-navigation-shared-element';
import { getUserInfo, reward } from '../../../api/services/account.service';
import { getAllBook } from '../../../api/services/book.service';
import { getApprovedPost } from '../../../api/services/post.service';
import { getTokenReward } from '../../../api/services/account.service';
import VideoAds from '../../../components/Ads/VideoAds';
import BList from '../../../components/Book/BookList';
import BListV2 from '../../../components/Book/BookListV2';
import { default as BText } from '../../../components/Text/Text';
import { AdmobContext } from '../../../context/AdmobContext';
import { AppContext } from '../../../context/AppContext';

const LottieViewAnimated = Animated.createAnimatedComponent(LottieView);

const studies = require('../../../anims/landscape.json');

const getGreeting = () => {
  const hours = new Date().getHours();
  if (hours < 12) {
    return 'Good Morning';
  }
  if (hours >= 12 && hours <= 17) {
    return 'Good Afternoon';
  }
  return 'Good Evening';
};

const Home = ({ navigation }) => {
  const { dark, width, colors, margin, navbar, normalize } = useTheme();
  const HEADER = normalize(300, 400);
  const [reading, setReading] = useState([]);
  const [completed, setCompleted] = useState([]);
  const [wishlist, setWishlist] = useState([]);
  const [bookList, setBookList] = useState([]);
  const [postList, setPostList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');

  const scrollY = useSharedValue(0);
  const loaded = useSharedValue(0);

  const { renderBanner, initRewardAds } = useContext(AdmobContext);
  const context = useContext(AppContext);

  const scrollHandler = useAnimatedScrollHandler({
    onScroll: ({ contentOffset }) => {
      scrollY.value = contentOffset.y;
    },
  });

  const loadData = async () => {
    const bookRes = (await getAllBook('quilting')).data;
    const postRes = (await getApprovedPost(10, 0, 'CreatedDate', 'DESC')).data;
    const books = bookRes.items;
    const posts = postRes.payload;
    setLoading(false);
    setPostList(posts);
    setBookList(books);

    await getCurrentUser();
  };

  const getCurrentUser = async () => {
    getUserInfo().then(async res => {
      if (res.data.statusCode === 200) {
        await AsyncStorage.setItem('@user', JSON.stringify(res.data.payload));
      }
    });
  };

  const watchAdsSuccess = () => {
    getTokenReward().then(res => {
      console.log('getTokenReward ~ res', res.data);
      if (res.data.statusCode === 200) {
        reward(res.data.payload, 0.02).then(res => {
          if (res.data.statusCode === 200) {
            const count = context.count;
            context.count = count - 1;
          }
        });
      }
    });
  };

  // Go to search screen
  const searchBooks = () => {
    if (searchTerm) {
      navigation.push('BookSearch', { searchTerm });
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      setLoading(true);
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  );

  useEffect(() => {
    if (bookList.length) {
      const [rList, cList, wList] = [[], [], []];
      bookList.forEach(bk => {
        switch (bk.volumeInfo.printType) {
          case 'MAGAZINE':
            cList.push(bk);
            break;
          case 'BOOK':
            rList.push(bk);
            break;
          default:
            wList.push(bk);
            break;
        }
        setReading(rList);
        setCompleted(cList);
        setWishlist(postList);
      });
    }
  }, [bookList]);

  const styles = StyleSheet.create({
    screen: {
      flex: 1,
      backgroundColor: colors.background,
    },
    header: {
      top: 50,
      left: 0,
      right: 0,
      zIndex: 10,
      height: HEADER,
      paddingTop: navbar,
      shadowRadius: 4,
      position: 'absolute',
      alignItems: 'center',
      justifyContent: 'flex-end',
      shadowOffset: { height: 2, width: 0 },
      backgroundColor: colors.background,
    },
    lottie: {
      alignSelf: 'center',
      height: '110%',
      marginLeft: 10,
      opacity: dark ? 0.8 : 1,
    },

    searchInput: {
      height: 50,
      marginBottom: -25,
      marginTop: margin / 2,
      width: width - margin * 2,
      borderWidth: 1,
      borderRadius: 25,
      paddingHorizontal: 20,
      justifyContent: 'center',
      backgroundColor: colors.card,
      borderColor: colors.background,
      position: 'relative',
      zIndex: 100,
    },
    searchIcon: {
      paddingRight: 10,
    },
    searchText: {
      color: '#000',
    },
    scrollView: {
      paddingTop: HEADER + 60,
    },
    addButton: {
      width: 60,
      height: 60,
      right: margin,
      bottom: margin,
      borderRadius: 60,
      position: 'absolute',
      backgroundColor: '#f78a37',
      zIndex: 101,
    },
    addIcon: {
      top: '35%',
      left: '35%',
      color: '#fff',
    },
  });

  const animatedStyles = {
    header: useAnimatedStyle(() => ({
      shadowOpacity: interpolate(
        scrollY.value,
        [0, HEADER - 100, HEADER - 80],
        [0, 0, 0.15],
        'clamp',
      ),
      transform: [
        {
          translateY: interpolate(
            scrollY.value,
            [0, HEADER - navbar],
            [0, -HEADER + navbar],
            'clamp',
          ),
        },
      ],
    })),
    logo: useAnimatedStyle(() => ({
      opacity: interpolate(scrollY.value, [0, HEADER - 100], [1, 0], 'clamp'),
      transform: [
        { translateY: interpolate(scrollY.value, [-200, 0], [50, 0], 'clamp') },
      ],
    })),
    lottieProps: useAnimatedProps(() => ({
      speed: 0.5,
      autoPlay: true,
      // progress: scrollY.value
      //   ? interpolate(scrollY.value, [-200, 0, 200], [0, 0.5, 1], 'clamp')
      //   : withTiming(1, { duration: 5000 }),
    })),
    welcome: useAnimatedStyle(() => ({
      transform: [
        {
          translateY: interpolate(scrollY.value, [-200, 0], [200, 0], 'clamp'),
        },
      ],
    })),
    welcomeText: useAnimatedStyle(() => ({
      transform: [
        {
          scale: interpolate(
            scrollY.value,
            [0, HEADER - 110, HEADER - 90],
            [1, 1, 0.85],
            'clamp',
          ),
        },
        {
          translateY: interpolate(
            scrollY.value,
            [0, HEADER - 110, HEADER - 90],
            [0, 0, 10],
            'clamp',
          ),
        },
      ],
    })),
  };

  const handleSearchChange = event => {
    setSearchTerm(event);
  };

  return (
    <View style={styles.screen}>
      <SharedElement
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          zIndex: 100,
          height: 70,
        }}>
        {renderBanner()}
      </SharedElement>
      <Animated.View style={[styles.header, animatedStyles.header]}>
        <Animated.View style={animatedStyles.logo}>
          <LottieViewAnimated
            source={studies}
            style={styles.lottie}
            animatedProps={animatedStyles.lottieProps}
          />
        </Animated.View>
        <Animated.View style={animatedStyles.welcome}>
          <BText animated style={animatedStyles.welcomeText} center size={20}>
            {getGreeting()}
          </BText>

          <SharedElement id="search">
            <Pressable onPress={searchBooks} style={styles.searchInput}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  backgroundColor: '#fff',
                }}>
                <AntDesign
                  color={colors.text}
                  name="search1"
                  size={20}
                  style={styles.searchIcon}
                />
                <TextInput
                  size={15}
                  style={styles.searchText}
                  placeholder="Tìm kiếm tài liệu..."
                  placeholderTextColor={'#555'}
                  onChangeText={handleSearchChange}
                  onSubmitEditing={searchBooks}></TextInput>
              </View>
            </Pressable>
          </SharedElement>
        </Animated.View>
      </Animated.View>

      {/* <TouchableOpacity
        onPress={() => initRewardAds()}
        style={styles.addButton}
        title="Press">
        <AntDesign size={21} name={'rocket1'} style={styles.addIcon} />
      </TouchableOpacity> */}
      {context.count > 0 && <VideoAds onAdsSuccess={watchAdsSuccess} />}
      {loading ? (
        <ActivityIndicator
          color={'#bf2e1b'}
          size={'large'}
          style={{
            position: 'absolute',
            top: '70%',
            left: '50%',
            zIndex: 1000,
          }}
        />
      ) : (
        <Animated.ScrollView
          scrollEventThrottle={8}
          onScroll={scrollHandler}
          contentContainerStyle={styles.scrollView}>
          <BListV2 books={wishlist} title="Mới" navigation={navigation} />
          {/* <BList books={reading} title="Đang đọc" navigation={navigation} />
          <BList books={completed} title="Hoàn thành" navigation={navigation} /> */}
        </Animated.ScrollView>
      )}
    </View>
  );
};
export default Home;
