import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect } from '@react-navigation/native';
import React, { useCallback, useEffect, useState } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import { useSharedValue } from 'react-native-reanimated';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getAllBook } from '../../../api/services/book.service';
import { getPostByResourceId } from '../../../api/services/post.service';
import { getActiveResource } from '../../../api/services/resource.service';
import BookList from '../../../components/Book/BookList';
import BookListV2 from '../../../components/Book/BookListV2';
import CustomHeader from '../../../components/CustomHeader';

const Library = ({ navigation }) => {
  const [search, setSearch] = useState('');
  const [bookList, setBookList] = useState([]);
  const [postList, setPostList] = useState([]);
  const [resources, setResources] = useState([]);
  const [resourceId, setResourceId] = useState('');
  const [loading, setLoading] = useState(false);
  const loaded = useSharedValue(0);

  const loadResource = useCallback(async () => {
    const resourcesRes = (await getActiveResource(10, 1, 'CreatedDate', 'desc'))
      .data;
    if (resourcesRes && resourcesRes.payload.length > 0) {
      setResources(resourcesRes.payload);
      setResourceId(resourceId ? resourceId : resourcesRes.payload[0].id);

      const postRes = (
        await getPostByResourceId(
          resourceId ? resourceId : resourcesRes.payload[0].id,
          0,
          10,
          'CreatedDate',
          'des',
        )
      ).data;
      const posts = postRes.payload;

      setPostList(posts);
      setLoading(false);
    }
  }, [resources, postList]);

  const refreshPost = useCallback(async () => {
    let postRes = null;

    if (!search) {
      postRes = (
        await getPostByResourceId(resourceId, 50, 0, 'CreatedDate', 'desc')
      ).data;
    } else {
      postRes = (
        await getPostByResourceId(
          resourceId,
          10,
          0,
          'CreatedDate',
          'desc',
          'Title',
          'contains',
          search,
        )
      ).data;
    }
    if (postRes) {
      setPostList(postRes.payload);
      setLoading(false);
    }
  }, [search, resourceId]);

  useEffect(() => {
    if (loaded.value === 1) {
      refreshPost();
    }
  }, [resourceId, search]);

  useFocusEffect(
    React.useCallback(() => {
      if (!loaded.value) {
        loadResource();
        loaded.value = 1;
      } else {
        setTimeout(loadResource, 450);
      }
    }, []),
  );

  const styles = StyleSheet.create({
    middle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });

  const handleSearchChange = search => {
    setLoading(true);
    setSearch(search);
  };

  const handleSelectItem = selected => {
    if (selected) {
      setLoading(true);
      setResourceId(selected);
    }
  };

  const Loading = () => (
    <View style={styles.middle}>
      <ActivityIndicator size="large" color={'red'} />
    </View>
  );

  if (resources && resourceId && resources.length > 0) {
    return (
      <View>
        <CustomHeader
          onSearchChange={handleSearchChange}
          onSelectedItem={handleSelectItem}
          items={resources}
          defaultValue={resourceId}
        />
        <SafeAreaView>
          {loading && <Loading />}
          {!loading && (
            <BookListV2
              books={postList}
              title=""
              navigation={navigation}
              horizontal={false}
            />
          )}
        </SafeAreaView>
      </View>
    );
  } else
    return (
      <ActivityIndicator
        style={{ position: 'absolute', top: '50%', left: '50%' }}
        size="large"
        color={'red'}
      />
    );
};

export default Library;
