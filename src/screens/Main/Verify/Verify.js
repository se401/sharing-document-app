import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect } from '@react-navigation/native';
import React, { useContext, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-elements';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { useSharedValue } from 'react-native-reanimated';
import { verify } from '../../../api/services/auth.service';
import { AppContext } from '../../../context/AppContext';

const Verify = ({navigation}) => {
  const context = useContext(AppContext);
  const [verifyToken, setVerifyToken] = useState('');
  const [code, setCode] = useState('');
  const loaded = useSharedValue(0);


  const loadData = async () => {
    const verifyToken = await AsyncStorage.getItem('@verifyToken');
    const data = verifyToken;
    if (data) {
      setVerifyToken(data)
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  )

  let navigateToLogin = () => navigation.navigate('Login')

  let handleSubmit = () => {
    verify(verifyToken, code).then((val) => {
      context.verifyToken = verifyToken;
      navigateToLogin();
    }).catch(e => {
      alert(e.message)
    });
  }

  return (
    <View style={[styles.loginContainer]}>
      <View style={styles.horizontal}>
        <View>
          <View style={styles.viewControl}>
            <Text style={styles.title}>
              Token:
            </Text>
            <TextInput value={verifyToken} editable={false} style={styles.input, {backgroundColor: '#3c3c3c', color: '#fff'}} placeholder="Enter Token" placeholderTextColor={'#333'} />
          </View>
          <View style={styles.viewControl}>
            <Text style={styles.title}>
              Code:
            </Text>
            <TextInput style={styles.input} onChangeText={(e) => setCode(e)} placeholderTextColor={'#333'}
              placeholder="Enter Code"
            />
          </View>
          <TouchableOpacity style={styles.btnContainer} onPress={handleSubmit}>
            <Text style={styles.text}>Verify</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  text: {
    color: '#000',
    fontSize: 20,
    color: '#fff',
    textAlign: 'center'
  },
  input: {
    width: '100%',
    color: '#333',
    borderWidth: 1,
    borderRadius: 10
  },
  loginContainer: {
    backgroundColor: '#c3c3c3',
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
    backgroundColor: '#fff'
  },
  imageContainer: {
    width: '100%',
    height: '100%'
  },
  viewControl: {
    flexDirection: 'column',
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    padding: 5,
  },
  title: {
    fontSize: 30,
    color: '#333',
    fontWeight: 'bold'
  },
  btnContainer: {
    padding: 10,
    backgroundColor: '#0000ff',
    color: '#333', borderRadius: 10,
    height: 50
  },
  linkContainer: {
    textAlign: 'center',
    margin: 10,
    fontSize: 18
  }
})
export default Verify;