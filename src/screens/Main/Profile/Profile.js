import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import { useFocusEffect, useTheme } from '@react-navigation/native';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import {
  Image,
  Pressable,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { ListItem } from 'react-native-elements';
import { Avatar } from 'react-native-elements/dist/avatar/Avatar';
import Animated, { useSharedValue } from 'react-native-reanimated';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import bookOpen from '../../../assets/images/book-open.png';
import VideoAds from '../../../components/Ads/VideoAds';
import Text from '../../../components/Text/Text';

import {
  getBalance,
  getTokenReward,
  getUserInfo,
  reward,
} from '../../../api/services/account.service';
import { AppContext } from '../../../context/AppContext';

const Profile = ({ navigation }) => {
  GoogleSignin.configure({
    webClientId:
      '664611043246-n865le7i1ijdkbki1mop01ge0kli64sb.apps.googleusercontent.com',
    offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
    scopes: [
      'email',
      'profile',
      'https://www.googleapis.com/auth/drive',
      'https://www.googleapis.com/auth/drive.file',
      'https://www.googleapis.com/auth/drive.appdata',
      'https://www.googleapis.com/auth/drive.metadata',
      'https://www.googleapis.com/auth/drive.readonly',
      'https://www.googleapis.com/auth/drive.metadata.readonly',
      'https://www.googleapis.com/auth/drive.apps.readonly',
      'https://www.googleapis.com/auth/drive.photos.readonly',
    ],
  });

  const { dark, width, colors, margin, navbar, normalize } = useTheme();
  const HEADER = normalize(150, 200);
  const BOOKW = normalize(130, 160);
  const BOOKH = BOOKW * 1.5;
  const context = useContext(AppContext);
  const [user, setUser] = useState();
  const [balance, setBalance] = useState(0);
  const [loading, setLoading] = useState(true);
  const [showBalance, setShowBalance] = useState(false);
  const loaded = useSharedValue(0);

  const styles = StyleSheet.create({
    header: {
      backgroundColor: '#FF6347',
      height: HEADER + 30,
    },
    iconImg: {
      width: 70,
      height: 70,
      borderRadius: 10,
      alignSelf: 'center',
      margin,
    },
    title: {
      textAlign: 'center',
      color: '#fff',
    },
    listTitle: {
      marginTop: margin - 15,
      marginBottom: margin - 10,
      marginLeft: margin,
      fontSize: 18,
      color: 'grey',
      textTransform: 'uppercase',
    },
    btnLogin: {
      marginTop: margin,
      marginBottom: margin,
      marginLeft: margin,
      flexDirection: 'row',
    },
    loginText: {
      paddingLeft: 20,
      margin: 'auto',
      fontSize: 18,
      color: '#FF6347',
    },
  });

  const goToLogin = () => {
    navigation.navigate('Login');
  };

  const logout = () => {
    AsyncStorage.clear().then(() => navigation.navigate('Login'));
  };

  const gotoAboutApp = () => {
    navigation.navigate('AboutApp');
  };

  const saveData = async (token = null, user = null) => {
    context.isLogin = true;
    await AsyncStorage.setItem('@ggToken', token);
    await AsyncStorage.setItem('@user', JSON.stringify(user || ''));
  };

  const gotoChangPassword = () => {
    navigation.navigate('ChangePassword');
  };

  const gotoInfo = () => {
    navigation.navigate('Info');
  };

  const openCreatePost = async () => {
    navigation.navigate('CreatePost', { isCreate: true });
  };

  const googleSignin = async () => {
    // if (context.isLogin) return;
    try {
      await GoogleSignin.hasPlayServices();
      const info = await GoogleSignin.signIn();
      console.warn({ userInfo: info });
      await saveData(info.idToken, info.user);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  const loadData = useCallback(async () => {
    const userRes = (await getUserInfo()).data;

    setUser(userRes.payload);

    await AsyncStorage.setItem('@user', JSON.stringify(userRes.payload));
    setLoading(false);
  }, []);

  const getBalanceFrom = useCallback(() => {
    getBalance()
      .then(res => {
        if (res.data.statusCode === 200) {
          setShowBalance(true);
          setBalance(res.data.payload);
        }
      })
      .catch(e => console.log('get balance err', e));
  });

  useFocusEffect(
    React.useCallback(() => {
      setLoading(true);
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  );

  const handleShowBalance = () => {
    let newState = !showBalance;
    setShowBalance(newState);
    if (newState) {
      getBalanceFrom();
    }
  };

  const watchAdsSuccess = () => {
    getTokenReward().then(res => {
      if (res.data.statusCode === 200) {
        console.log('reward ~ res.data.payload', res.data.payload);
        reward(res.data.payload, 0.02).then(res => {
          if (res.data.statusCode === 200) {
            const count = context.count;
            context.count = count - 1;
          }
        }).then(() => getBalanceFrom());
      }
    });
  };

  return (
    <SafeAreaView>
      <Animated.ScrollView>
        <View style={{ flex: 1 }}>
          <Pressable>
            <View style={styles.header}>
              <Image source={bookOpen} style={styles.iconImg} />
              {!context.isLogin ? (
                <Text bold size={27} style={styles.title}>
                  Đăng nhập
                </Text>
              ) : (
                <Text bold size={25} style={styles.title}>
                  Sharing App {'\n'}
                  {user && user.displayName}
                </Text>
              )}
            </View>
          </Pressable>
          <View>
            <Text style={styles.listTitle}>Ví</Text>

            <View>
              <TouchableOpacity onPress={handleShowBalance}>
                <ListItem bottomDivider>
                  <Avatar
                    source={{
                      uri: 'https://www.mcicon.com/wp-content/uploads/2020/12/Business_Finance_Wallet_1-copy-5.jpg',
                    }}
                  />
                  <ListItem.Content>
                    <ListItem.Title>
                      {showBalance ? `${balance} ELON` : '********'}
                    </ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Text style={styles.listTitle}>Hoạt động</Text>

            <View>
              <TouchableOpacity onPress={openCreatePost}>
                <ListItem bottomDivider>
                  <Avatar
                    source={{
                      uri: 'http://s3-hcm-r1.longvan.net/efe/google-drive-icon.png',
                    }}
                  />
                  <ListItem.Content>
                    <ListItem.Title>Thêm sách</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              </TouchableOpacity>
              {/* <TouchableOpacity onPress={googleSignin}>
                <ListItem bottomDivider>
                  <Avatar
                    source={{
                      uri: 'https://cdn2.iconfinder.com/data/icons/picons-basic-1/57/basic1-111_user_security-128.png',
                    }}
                  />
                  <ListItem.Content>
                    <ListItem.Title>Cấp quyền Drive</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              </TouchableOpacity> */}
            </View>
          </View>
          <Text style={styles.listTitle}>Thông tin</Text>
          <View>
            <TouchableOpacity onPress={gotoAboutApp}>
              <ListItem bottomDivider>
                <AntDesign name={'infocirlceo'} size={24} />
                <ListItem.Content>
                  <ListItem.Title>
                    <Text>Về ứng dụng</Text>
                  </ListItem.Title>
                </ListItem.Content>
              </ListItem>
            </TouchableOpacity>
            {context.isLogin && (
              <TouchableOpacity onPress={gotoChangPassword}>
                <ListItem bottomDivider>
                  <MaterialIcons name={'security'} size={24} />
                  <ListItem.Content>
                    <ListItem.Title>Đổi mật khẩu</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              </TouchableOpacity>
            )}
            {context.isLogin && (
              <TouchableOpacity onPress={gotoInfo}>
                <ListItem bottomDivider>
                  <MaterialIcons name={'verified-user'} size={24} />
                  <ListItem.Content>
                    <ListItem.Title>Thông tin cá nhân</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              </TouchableOpacity>
            )}
          </View>
          {!context.isLogin ? (
            <TouchableOpacity style={styles.btnLogin} onPress={goToLogin}>
              <AntDesign name={'login'} size={24} color={'#000'} />
              <Text style={styles.loginText}>Đăng nhập</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity style={styles.btnLogin} onPress={logout}>
              <AntDesign name={'login'} size={24} color={'#FF6347'} />
              <Text style={styles.loginText}>Đăng xuất</Text>
            </TouchableOpacity>
          )}
          {context.count > 0 && <VideoAds onAdsSuccess={watchAdsSuccess} />}
        </View>
      </Animated.ScrollView>
    </SafeAreaView>
  );
};

export default Profile;
