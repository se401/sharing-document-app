import React from 'react';
import { StyleSheet, View } from 'react-native';

import Text from '../../../../components/Text/Text';

const AboutApp = () => {
  return (
    <View style={styles.container}>
      <Text size={20} style={styles.title}>
        Sharing App for Android v1.0
      </Text>
      <Text>Đây là ứng dụng chia sẻ tài liệu thông qua google drive</Text>
      <Text>Ứng dụng bao gồm các chức năng chính</Text>
      <Text>- Tìm kiếm tài liệu</Text>
      <Text>- Chia sẻ tài liệu</Text>
      <Text>- Bình luận đánh giá</Text>
      <Text>- Thanh toán một số tài liệu cao cấp</Text>
      <Text>
        Mục tiêu ứng dụng tạo ra một hệ thống nhằm lưu trữ tất cả các link về
        các khóa học các nguồn tài liệu trong lĩnh vực lập trình. Nhằm hỗ trợ
        các bạn mới bước vào nghành lập trình có nguồn tài liệu đa dạng để học
        hỏirf
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 20,
    paddingLeft: 10,
  },
  title: {
    fontWeight: 'bold',
  },
});

export default AboutApp;
