import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect } from '@react-navigation/native';
import React, { useCallback, useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import {
  ActivityIndicator,
  Alert,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import Clipboard from '@react-native-clipboard/clipboard';
import { TextInput } from 'react-native-gesture-handler';
import Animated, { useSharedValue } from 'react-native-reanimated';
import {
  getUserInfo,
  updateAccountInfo,
} from '../../../../api/services/account.service';
import Text from '../../../../components/Text/Text';
const Info = ({ navigation }) => {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);
  const loaded = useSharedValue(0);

  const { handleSubmit, control, setValue } = useForm();

  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    middle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    text: {
      color: '#000',
      fontSize: 18,
      color: '#fff',
      textAlign: 'center',
    },
    input: {
      width: 300,
      color: '#333',
      borderWidth: 1,
      borderRadius: 10,
      borderColor: 'black',
    },
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      backgroundColor: '#fff',
    },
    imageContainer: {
      width: '100%',
      height: '100%',
    },
    viewControl: {
      flexDirection: 'column',
      width: '100%',
      marginTop: 7,
      marginBottom: 7,
    },
    title: {
      fontSize: 27,
      color: '#333',
      fontWeight: 'bold',
    },
    btnContainer: {
      padding: 10,
      backgroundColor: '#0000ff',
      color: '#333',
      borderRadius: 10,
    },
    btnContainerCancel: {
      padding: 10,
      backgroundColor: '#c3c3c3',
      color: '#333',
      borderRadius: 10,
      marginRight: 10,
    },
    textStyle: {
      padding: 10,
      color: 'black',
      textAlign: 'center',
    },
    buttonStyle: {
      alignItems: 'center',
      backgroundColor: '#DDDDDD',
      padding: 5,
      marginVertical: 10,
      width: '100%',
    },
    imageStyle: {
      width: 200,
      height: 200,
      margin: 5,
    },
  });

  const Loading = () => (
    <View style={styles.middle}>
      <ActivityIndicator size="large" color={'red'} />
    </View>
  );

  const loadData = useCallback(async () => {
    const userRes = (await getUserInfo()).data;

    setUser(userRes.payload);
    console.log('loadData ~ userRes.payload', userRes.payload);
    await AsyncStorage.setItem('@user', JSON.stringify(userRes.payload));
    setLoading(false);
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  );

  useEffect(() => {
    if (user) {
      setValue('address', user.address, { shouldValidate: true });
      setValue('displayName', user.displayName, { shouldValidate: true });
    }
  }, [user]);

  const onSubmit = data => {
    console.log(data, 'data');

    updateAccountInfo(data.address, data.displayName).then(res => {
      if (res.data.statusCode === 200) {
        Alert.alert(
          'Thành công',
          'Cập nhật thành công',
          [
            {
              text: 'Hủy',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: 'Xác nhận',
              onPress: async () => {
                navigation.goBack();
              },
            },
          ],
          {
            cancelable: true,
          },
        );
      }
    });
  };

  const handleBack = () => navigation.goBack();

  const copyAddress = () => {
    Clipboard.setString(user.walletAddress);
    Alert.alert('Thành công', 'Sao chép thành công');
  };

  if (!loading) {
    return (
      <SafeAreaView>
        <Animated.ScrollView>
          <View style={styles.container}>
            <View style={styles.horizontal}>
              <View>
                <TouchableOpacity
                  onLongPress={copyAddress}
                  style={styles.viewControl}>
                  <Text style={styles.title}>ID:</Text>
                  <Text>{user.walletAddress}</Text>
                </TouchableOpacity>
                <View style={styles.viewControl}>
                  <Text style={styles.title}>Địa chỉ:</Text>
                  <Controller
                    name="address"
                    control={control}
                    defaultValue={null}
                    render={({ value, field, fieldState }) => (
                      <View>
                        <TextInput
                          onChangeText={text => field.onChange(text)}
                          value={field.value}
                          style={[
                            styles.input,
                            {
                              borderColor: fieldState.invalid
                                ? '#f44336'
                                : 'black',
                            },
                          ]}
                          placeholder="Nhập địa chỉ"
                          placeholderTextColor={'#333'}
                        />
                        {/* {fieldState.invalid && <Text style={{ color: '#f44336' }}>{fieldState.error?.message}</Text>} */}
                      </View>
                    )}
                  />
                </View>
                <View style={styles.viewControl}>
                  <Text style={styles.title}>Tên hiển thị:</Text>
                  <Controller
                    name="displayName"
                    control={control}
                    defaultValue={null}
                    rules={{
                      required: {
                        value: true,
                        message: 'Tên hiển thị là bắt buộc',
                      },
                    }}
                    render={({ value, field, fieldState }) => (
                      <View>
                        <TextInput
                          onChangeText={text => field.onChange(text)}
                          value={field.value}
                          style={[
                            styles.input,
                            {
                              borderColor: fieldState.invalid
                                ? '#f44336'
                                : 'black',
                            },
                          ]}
                          placeholder="Nhập tên hiển thị"
                          placeholderTextColor={'#333'}
                        />
                        {fieldState.invalid && (
                          <Text style={{ color: '#f44336' }}>
                            {fieldState.error?.message}
                          </Text>
                        )}
                      </View>
                    )}
                  />
                </View>
                <View
                  style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <TouchableOpacity
                    style={styles.btnContainerCancel}
                    onPress={handleBack}>
                    <Text style={(styles.text, { color: 'black' })}>Back</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.btnContainer}
                    onPress={handleSubmit(onSubmit)}>
                    <Text style={styles.text}>Upload</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Animated.ScrollView>
      </SafeAreaView>
    );
  } else return <Loading />;
};

export default Info;
