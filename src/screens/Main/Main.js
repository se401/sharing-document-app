import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Activity from './Activity/Activity';
import Category from './Category/Category';
import Home from './Home/Home';
import Library from './Library/Library';
import Profile from './Profile/Profile';

const MainTab = createBottomTabNavigator();

const MainRouter = () => (
  <MainTab.Navigator initialRouteName={'Home'} tabBarOptions={{
    activeTintColor: '#FF6347',
  }}>
    <MainTab.Screen name={'Home'} component={Home} options={{
      tabBarLabel: 'Trang chủ',
      tabBarIcon: ({ color, size }) => (
        <AntDesignIcon
          name="appstore1"
          color={color}
          size={size}
        />
      ),
    }}></MainTab.Screen>
    <MainTab.Screen name={'Category'} component={Category} options={{
      tabBarLabel: 'Danh mục',
      tabBarIcon: ({ color, size }) => (
        <SimpleLineIcons
          name="globe"
          color={color}
          size={size}
        />
      ),
    }}></MainTab.Screen>
    <MainTab.Screen name={'Library'} component={Library} options={{
      tabBarLabel: 'Nguồn',
      tabBarIcon: ({ color, size }) => (
        <Ionicons
          name="layers"
          color={color}
          size={size}
        />
      ),
    }}></MainTab.Screen>
    <MainTab.Screen name={'Activity'} component={Activity} options={{
      tabBarLabel: 'Hoạt động',
      tabBarIcon: ({ color, size }) => (
        <AntDesignIcon
          name="barschart"
          color={color}
          size={size}
        />
      ),
    }}></MainTab.Screen>
    <MainTab.Screen name={'Profile'} component={Profile} options={{
      tabBarLabel: 'Cá nhân',
      tabBarIcon: ({ color, size }) => (
        <FontAwesomeIcons
          name="user-circle-o"
          color={color}
          size={size}
        />
      ),
    }}></MainTab.Screen>
  </MainTab.Navigator>
)
export default MainRouter;