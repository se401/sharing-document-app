import { useFocusEffect } from '@react-navigation/native';
import React, { useCallback, useEffect, useState } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  useWindowDimensions,
  View,
} from 'react-native';
import { Tab } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import Animated, { useSharedValue } from 'react-native-reanimated';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getActiveCategories } from '../../../api/services/category.service';
import { getPostByCateId } from '../../../api/services/post.service';
import BookListV2 from '../../../components/Book/BookListV2';
import CustomHeader from '../../../components/CustomHeader';

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

const Category = ({ navigation }) => {
  const [search, setSearch] = useState('');

  const [activeTab, setActiveTab] = useState(0);
  const [categories, setCategories] = useState([]);
  const mywindow = useWindowDimensions();
  const [loading, setLoading] = useState(false);
  const [sortField, setSortField] = useState(null);
  const [postList, setPostList] = useState(null);
  const [cateId, setCateId] = useState('');
  const loaded = useSharedValue(0);

  const loadData = useCallback(async () => {
    // const bookRes = (await getAllBook(activeTab)).data;
    const categoriesRes = (
      await getActiveCategories(50, 1, sortField || 'CreatedDate', 'desc')
    ).data;

    if (categoriesRes && categoriesRes.payload.length > 0) {
      console.log('loadData ~ activeTab', activeTab);
      console.log('loadData ~ sortField', sortField);
      setCategories(categoriesRes.payload);
      setCateId(cateId ? cateId : categoriesRes.payload[0].id);

      const postRes = (
        await getPostByCateId(
          cateId ? cateId : categoriesRes.payload[0].id,
          10,
          0,
          'CreatedDate',
          'desc',
        )
      ).data;
      setPostList(postRes.payload);
      setLoading(false);
    }
  }, []);

  const refreshPost = useCallback(async () => {
    let postRes = null;
    let sortFieldTemp = '';

    switch (activeTab) {
      case 0:
        sortFieldTemp = 'CreatedDate';
        break;
      case 1:
        sortFieldTemp = 'ViewCount';
        break;
      default:
        sortFieldTemp = 'LikeCount';
        break;
    }
    if (!search) {
      postRes = (await getPostByCateId(cateId, 50, 0, sortFieldTemp, 'desc'))
        .data;
    } else {
      postRes = (
        await getPostByCateId(
          cateId,
          10,
          0,
          sortFieldTemp,
          'asc',
          'Title',
          'contains',
          search,
        )
      ).data;
    }

    if (postRes) {
      setPostList(postRes.payload);
      setLoading(false);
      setSortField(sortFieldTemp)
    }
  }, [activeTab, search, cateId]);

  useFocusEffect(
    React.useCallback(() => {
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  );

  useEffect(() => {
    if (loaded.value === 1) {
      refreshPost();
    }
  }, [cateId, activeTab, search]);

  const styles = StyleSheet.create({
    tabContentContainer: {},
    tabContent: {
      justifyContent: 'center',
      height: mywindow.height * 0.8,
    },
  });

  const changeTab = value => {
    setLoading(true);
    setActiveTab(value);
  };

  const handleSearchChange = search => {
    setLoading(true);
    setSearch(search);
  };

  const handleSelectItem = selected => {
    setLoading(true);
    setSearch('');
    setCateId(selected);
  };

  if (categories && cateId && categories.length > 0) {
    return (
      <View>
        <CustomHeader
          onSearchChange={handleSearchChange}
          onSelectedItem={handleSelectItem}
          items={categories}
          defaultValue={cateId}
        />

        <Tab onChange={changeTab} value={activeTab}>
          <Tab.Item
            title="Mới"
            titleStyle={{
              fontSize: 13,
              color: activeTab === 0 ? '#FF6347' : '#000',
            }}
            containerStyle={{ backgroundColor: 'white' }}
          />
          <Tab.Item
            title="Nổi bật"
            titleStyle={{
              fontSize: 13,
              color: activeTab === 1 ? '#FF6347' : '#000',
            }}
            containerStyle={{ backgroundColor: 'white' }}
          />
          <Tab.Item
            title="Hot"
            titleStyle={{
              fontSize: 13,
              color: activeTab === 2 ? '#FF6347' : '#000',
            }}
            containerStyle={{ backgroundColor: 'white' }}
          />
        </Tab>
        <SafeAreaView>
          <View style={styles.tabContentContainer}>
            {postList && !loading ? (
              <BookListV2
                books={postList}
                title=""
                navigation={navigation}
                horizontal={false}
              />
            ) : (
              <View style={styles.tabContent}>
                <ActivityIndicator size="large" color={'red'} />
              </View>
            )}
          </View>
        </SafeAreaView>
      </View>
    );
  } else return <></>;
};
export default Category;
