import AsyncStorage from '@react-native-async-storage/async-storage';
import { Link, useFocusEffect } from '@react-navigation/native';
import jwtDecode from 'jwt-decode';
import React, { useContext, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { useSharedValue } from 'react-native-reanimated';
import { login } from '../../api/services/auth.service';
import { AppContext } from '../../context/AppContext';

const Login = ({ navigation }) => {
  const context = useContext(AppContext);
  const [loading, setLoading] = useState(false);
  const loaded = useSharedValue(0);
  const { handleSubmit, control, setValue } = useForm();

  const saveData = async (token = null, decode = null, email = null, password = null) => {
    console.log('saveData ~ { email, password }', { email, password });
    await AsyncStorage.setItem('@auth', JSON.stringify({ email, password }));

    await AsyncStorage.setItem('@token', token);
    await AsyncStorage.setItem('@decode', JSON.stringify(decode));
  };

  const loadData = async () => {
    setLoading(true);
    const json = await AsyncStorage.getItem('@auth');
    const data = json ? JSON.parse(json) : null;
    console.log('loadData ~ data', data);
    if (data && data.email && data.password) {
      if (data.email && data.password) {
        login(data.email, data.password)
          .then(val => {
            if (val.data) {
              context.isLogin = true;
              const decode = jwtDecode(val?.data.payload.accessToken);
              console.log(
                'LOG ~ file: Login.js ~ line 38 ~ login ~ decode',
                decode,
              );
              saveData(val.data.payload.accessToken, decode, data.email, data.password);
              navigateToHome();
            }
            setLoading(false);
          })
          .catch(e => {
            console.log('loadData ~ e', e);
            setLoading(false);
          });
      }
    } else {
      setLoading(false);
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  );

  let navigateToHome = () => navigation.navigate('Main');

  let handleSubmitLogin = data => {
    console.log('Login ~ data', data);
    setLoading(true);
    if (data.email && data.password) {
      login(data.email, data.password)
        .then(val => {
          context.isLogin = true;
          setLoading(false);
          const decode = jwtDecode(val.data.payload.accessToken);
          saveData(val.data.payload.accessToken, decode, data.email, data.password);
          navigateToHome();
        })
        .catch(e => {
          console.log('handleSubmit ~ e', e);
          setLoading(false);
        });
    } else {
      setLoading(false);
    }
  };

  return !loading ? (
    <View style={[styles.loginContainer]}>
      <View style={styles.horizontal}>
        <View>
          <View style={styles.viewControl}>
            <Text style={styles.title}>
              Email <Text style={{ color: 'red' }}>*</Text>:
            </Text>
            <Controller
              name="email"
              control={control}
              defaultValue={null}
              rules={{
                required: {
                  value: true,
                  message: 'Email bắt buộc',
                },
                pattern: {
                  value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
                  message: 'Email sai định dạng',
                },
              }}
              render={({ value, field, fieldState }) => (
                <View>
                  <TextInput
                    onChangeText={text => field.onChange(text)}
                    value={value}
                    style={[
                      styles.input,
                      {
                        borderColor: fieldState.invalid ? '#f44336' : 'black',
                      },
                    ]}
                    placeholder="Nhập email"
                    placeholderTextColor={'#333'}
                  />
                  {fieldState.invalid && (
                    <Text style={{ color: '#f44336' }}>
                      {fieldState.error?.message}
                    </Text>
                  )}
                </View>
              )}
            />
          </View>
          <View style={styles.viewControl}>
            <Text style={styles.title}>
              Mật khẩu <Text style={{ color: 'red' }}>*</Text>:
            </Text>
            <Controller
              name="password"
              control={control}
              defaultValue={null}
              rules={{
                required: {
                  value: true,
                  message: 'Mật khẩu bắt buộc',
                },
              }}
              render={({ value, field, fieldState }) => (
                <View>
                  <TextInput
                    onChangeText={text => field.onChange(text)}
                    secureTextEntry
                    value={value}
                    style={[
                      styles.input,
                      {
                        borderColor: fieldState.invalid ? '#f44336' : 'black',
                      },
                    ]}
                    placeholder="Nhập mật khẩu"
                    placeholderTextColor={'#333'}
                  />
                  {fieldState.invalid && (
                    <Text style={{ color: '#f44336' }}>
                      {fieldState.error?.message}
                    </Text>
                  )}
                </View>
              )}
            />
          </View>
          <TouchableOpacity
            style={styles.btnContainer}
            onPress={handleSubmit(handleSubmitLogin)}>
            <Text style={styles.text}>Login</Text>
          </TouchableOpacity>
          <Link style={styles.linkContainer} to={'/Register'}>
            Register
          </Link>
        </View>
      </View>
    </View>
  ) : (
    <View style={styles.middle}>
      <ActivityIndicator size="large" color={'red'} />
    </View>
  );
};

const styles = StyleSheet.create({
  middle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#000',
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
  },
  input: {
    width: 300,
    color: '#333',
    borderWidth: 1,
    borderRadius: 10,
  },
  loginContainer: {
    backgroundColor: '#c3c3c3',
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
    backgroundColor: '#fff',
  },
  imageContainer: {
    width: '100%',
    height: '100%',
  },
  viewControl: {
    flexDirection: 'column',
    width: '100%',
    marginTop: 10,
    marginBottom: 10,
    padding: 5,
  },
  title: {
    fontSize: 30,
    color: '#333',
    fontWeight: 'bold',
  },
  btnContainer: {
    padding: 10,
    backgroundColor: '#0000ff',
    color: '#333',
    borderRadius: 10,
    height: 50,
  },
  linkContainer: {
    textAlign: 'center',
    margin: 10,
    fontSize: 18,
  },
});

export default Login;
