import React, { useEffect } from 'react';
import { ActivityIndicator, ImageBackground, StyleSheet, View } from 'react-native';
import images from '../../assets/images/images';

const Loading = ({ navigation }) => {

  useEffect(() => {
    const timeworker = setTimeout(
      () => navigation.navigate('Main'),
      3000
    )
    return () => {
      clearTimeout(timeworker)
    };
  }, [])

  return (
    <ImageBackground source={images.otherStars} style={styles.imageContainer}>
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#dd00dd" />
      </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  imageContainer: {
    width: '100%',
    height: '100%'
  }
})


export default Loading;