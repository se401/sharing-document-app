import React, { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Alert, SafeAreaView, StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';
import Animated, { useSharedValue } from 'react-native-reanimated';
import { useTheme } from 'react-navigation';
import { changePassword } from '../../api/services/account.service';
import Text from '../../components/Text/Text';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect } from '@react-navigation/native';



const ChangePassword = ({ navigation }) => {
  const [password, setPassword] = useState('');
  const loaded = useSharedValue(0);


  const { handleSubmit, control } = useForm();
  const {
    height
  } = useTheme();

  const loadData = async () => {
    let infoJson = await AsyncStorage.getItem('@auth');
    const { email, password } = JSON.parse(infoJson);
    setPassword(password);
  };

  useFocusEffect(
    React.useCallback(() => {
      if (!loaded.value) {
        loadData();
        loaded.value = 1;
      } else {
        setTimeout(loadData, 450);
      }
    }, []),
  )

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",

    },
    text: {
      color: '#000',
      fontSize: 18,
      color: '#fff',
      textAlign: 'center'
    },
    input: {
      width: 300,
      color: '#333',
      borderWidth: 1,
      borderRadius: 10,
      borderColor: 'black'
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      backgroundColor: '#fff',
    },
    imageContainer: {
      width: '100%',
      height: '100%'
    },
    viewControl: {
      flexDirection: 'column',
      width: '100%',
      marginTop: 7,
      marginBottom: 7,
    },
    title: {
      fontSize: 27,
      color: '#333',
      fontWeight: 'bold'
    },
    btnContainer: {
      padding: 10,
      backgroundColor: '#0000ff',
      color: '#333',
      borderRadius: 10,
    },
    btnContainerCancel: {
      padding: 10,
      backgroundColor: '#c3c3c3',
      color: '#333',
      borderRadius: 10,
      marginRight: 10
    },
    textStyle: {
      padding: 10,
      color: 'black',
      textAlign: 'center',
    },
    buttonStyle: {
      alignItems: 'center',
      backgroundColor: '#DDDDDD',
      padding: 5,
      marginVertical: 10,
      width: '100%',
    },
    imageStyle: {
      width: 200,
      height: 200,
      margin: 5,
    },
  })

  const handleBack = () => navigation.goBack();
  const onSubmit = (data) => {
    const { old_password, new_password, confirm_password } = data;
    console.log('LOG ~ file: ChangePassword.js ~ line 92 ~ onSubmit ~ confirm_password === new_password', confirm_password === new_password)

    if(password !== old_password){
      alert('Mật khẩu cũ không đúng');
      return;
    }

    if (confirm_password !== new_password) {
      alert('Mật khẩu xác nhận không đúng');
      return;
    }

    // Change password
    changePassword(new_password, confirm_password).then(
      res => {
        if (res.data.statusCode === 200) {
          Alert.alert(
            'Thành công',
            'Đổi mật khẩu thành công',
            [
              {
                text: "Hủy",
                onPress: () => console.log('Cancel Pressed'),
                style: "cancel",
              },
              {
                text: "Xác nhận",
                onPress: () => navigation.navigate('Home')
              },
            ],
            {
              cancelable: true
            }
          )
        }
      }
    )
  }

  return (
    <SafeAreaView>
      <Animated.ScrollView>
        <View style={styles.container}>
          <View style={styles.horizontal}>
            <View>
              <View style={styles.viewControl}>
                <Text style={styles.title}>
                  Mật khẩu cũ:
                </Text>
                <Controller
                  name="old_password"
                  control={control}
                  defaultValue={null}
                  rules={{
                    required: { value: true, message: 'Old password is required' }
                  }}
                  render={({ value, field, fieldState }) => (
                    <View>
                      <TextInput onChangeText={(text) => field.onChange(text)} secureTextEntry={true}
                        value={value} style={[styles.input, { borderColor: fieldState.invalid ? '#f44336' : 'black' }]}
                        placeholder="Enter Old Password" placeholderTextColor={'#333'} />
                      {fieldState.invalid && <Text style={{ color: '#f44336' }}>{fieldState.error?.message}</Text>}
                    </View>
                  )}
                />
              </View>
              <View style={styles.viewControl}>
                <Text style={styles.title}>
                  Mật khẩu mới:
                </Text>
                <Controller
                  name="new_password"
                  control={control}
                  defaultValue={null}
                  rules={{
                    required: { value: true, message: 'New password is required' }
                  }}
                  render={({ value, field, fieldState }) => (
                    <View>
                      <TextInput onChangeText={(text) => field.onChange(text)} secureTextEntry={true}
                        value={value} style={[styles.input, { borderColor: fieldState.invalid ? '#f44336' : 'black' }]}
                        placeholder="Enter New Password" placeholderTextColor={'#333'} />
                      {fieldState.invalid && <Text style={{ color: '#f44336' }}>{fieldState.error?.message}</Text>}
                    </View>
                  )}
                />
              </View>
              <View style={styles.viewControl}>
                <Text style={styles.title}>
                  Xác nhận mật khẩu:
                </Text>
                <Controller
                  name="confirm_password"
                  control={control}
                  defaultValue={null}
                  rules={{
                    required: { value: true, message: 'Confirm password is required' }
                  }}
                  render={({ value, field, fieldState }) => (
                    <View>
                      <TextInput onChangeText={(text) => field.onChange(text)} secureTextEntry={true}
                        value={value} style={[styles.input, { borderColor: fieldState.invalid ? '#f44336' : 'black' }]}
                        placeholder="Enter Confirm Password" placeholderTextColor={'#333'} />
                      {fieldState.invalid && <Text style={{ color: '#f44336' }}>{fieldState.error?.message}</Text>}
                    </View>
                  )}
                />
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <TouchableOpacity style={styles.btnContainerCancel} onPress={handleBack}>
                  <Text style={styles.text, { color: 'black' }}>Hủy</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnContainer} onPress={handleSubmit(onSubmit)}>
                  <Text style={styles.text}>Xác nhận</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Animated.ScrollView>
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({});

export default ChangePassword;