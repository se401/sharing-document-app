import { StyleSheet } from 'react-native';

export const globalVariable = StyleSheet.create({
  activeColor: {
    color: '#bf2e1b'
  },
  hoverColor: {
    color: '#f78a37'
  }
});