const images = {
  otherStars: require('./other-stars.jpg'),
  author: require('./author.jpg'),
  testImage: require('./documents/tieuhongtran.jpg')
}

export default images
