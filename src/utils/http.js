import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { baseUrl } from './base-url.constant';
import { errorResponseHandler } from './error';

const instance = axios.create({
  baseURL: baseUrl,
  timeout: 3000,
});

instance.interceptors.request.use(
  async config => {
    let token = await AsyncStorage.getItem('@token');
    if (token) {
      config.headers.authorization = `Bearer ${token}`;
    }
    return config;
  },
  error => Promise.reject(error),
);

instance.interceptors.response.use(
  response => {
    const { data } = response;
    if (data.statusCode !== 200) {
      throw new Error(data.message);
    }
    return response;
  },
  ({ message, response: { data, status } }) => {
    console.log('data', message);
    errorResponseHandler(data.message);
    return Promise.reject({ message, data, status });
  },
);

export default http = instance;
