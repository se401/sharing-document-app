import React from 'react';
import { Alert } from 'react-native';

export const errorHandler = (e, isFatal) => {
  if (isFatal) {
    Alert.alert(
      'Unexpected error occurred',
      `
        Error: ${isFatal ? 'Fatal:' : ''} ${e.name} ${e.message}
        We have reported this to our team ! Please close the app and start again!
        `,
      [
        {
          text: 'Close',
        },
      ],
    );
  } else {
    console.log(e); // So that we can see it in the ADB logs in case of Android if needed
  }
};

export const errorResponseHandler = message => {
  Alert.alert('Thông báo', message || 'Lỗi máy chủ', [
    {
      text: 'Close',
    },
  ]);
};
