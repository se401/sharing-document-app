import axios from 'axios';

export const getAllBook = (q, offset = 0, limit = 10) => axios.get(`https://www.googleapis.com/books/v1/volumes?q=${q}&startIndex=${offset * limit}&maxResults=${limit}`).catch(e => console.log('getAllBook', e))

export const getBookById = (id) => axios.get('https://www.googleapis.com/books/v1/volumes/' + id).catch(e => console.log('getBookById', e))