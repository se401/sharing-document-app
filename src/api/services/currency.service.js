import { baseUrl } from '../../utils/base-url.constant';
import http from '../../utils/http';

export const convertCurrency = (sourceCurrency, amount) => {
  return http.post(`${baseUrl}/Util/convert-currency`, {
    sourceCurrency,
    amount,
  });
};
