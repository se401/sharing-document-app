import { baseUrl } from '../../utils/base-url.constant';
import http from '../../utils/http';



export const login = (email, password) => http.post(`${baseUrl}/Auth/login`, { email, password })


export const register = (
  email, password, displayName, username, phoneNumber, address
) => {
  return http.post(`${baseUrl}/Auth/register`, {
    email, password, displayName, username, phoneNumber, address
  })
}


export const verify = (token, code) => http.post(`${baseUrl}/Auth/verify`, { token, code })