import { baseUrl } from '../../utils/base-url.constant';
import http from '../../utils/http';

/*
public string Text { get; set; }
public CommentResp ReplyTo { get; set; }
public string PostId { get; set; }
public bool IsReported { get; set; }
public string Reason { get; set; }
public AccountResp User { get; set; }
*/

export const createComment = (message, replyCommentId, postId) => {
  return http.post(`${baseUrl}/Comment/create`, { message, replyCommentId, postId });
}

export const editComment = (text, commentId) => {
  return http.post(`${baseUrl}/Comment/edit`, { text, commentId });
}

export const getCommentsByPost = (postId) => {
  return http.get(`${baseUrl}/Comment/get-comments/${postId}`);
}

export const reportComment = (commentId, reasons) => { //reasons: string[]
  return http.post(`${baseUrl}/Comment/report-comments`, { commentId, reasons });
}

export const blockComment = (commentId) => { //reasons: string[]
  return http.post(`${baseUrl}/Comment/mark-as-blocked/${commentId}`);
}