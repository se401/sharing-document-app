import http from '../../utils/http';

const API_KEY = 'e05fe595be2ae4004770b05593e2df80';

export const getByCityName = (cityName) => {
  return http.get(`https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=${API_KEY}`)
}

export const getByCoordinates = (lat, lon) => {
  return http.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${API_KEY}`)
}