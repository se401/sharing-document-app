import { baseUrl } from '../../utils/base-url.constant';
import http from '../../utils/http';
import * as qs from 'qs';


export const createCategory = (name, description, parentId, file) => {

  const body = new FormData();

  body.set('Name', name);
  body.set('Description', description);
  body.set('ParentId', parentId);
  body.set('file', file);

  return http.post(
    `${baseUrl}/Category/create-category`,
    body,
    { headers: { 'Content-Type': 'multipart/form-data' } }
  ).catch(e => console.log('createCategory', e))
}

export const updateCategory = (id, name, description, parentId, file) => {

  const body = new FormData();

  body.set('Name', name);
  body.set('Description', description);
  body.set('ParentId', parentId);
  body.set('file', file);

  return http.post(
    `${baseUrl}/Category/update-category/${id}`,
    body,
    { headers: { 'Content-Type': 'multipart/form-data' } }).catch(e => console.log('updateCategory', e))
}

export const getCategories = (pageSize, page, sortField = null, sortOrder = null, filterField = null, filterOperator = null, filterValue = null) => {
  const query = qs.stringify({
    PageSize: pageSize,
    PageNumber: page,
    SortField: sortField,
    SortOrder: sortOrder,
    FilterField: filterField,
    FilterOperator: filterOperator,
    FilterValue: filterValue
  }, { skipNulls: true });
  return http.get(`${baseUrl}/Category/get-categories`, { params: query }).catch(e => console.log('getCategories', e))
}

export const getActiveCategories = (pageSize, page, sortField = null, sortOrder = null, filterField = null, filterOperator = null, filterValue = null) => {
  let query = `PageSize=${pageSize}&PageNumber=${page}`;
  if (sortField) {
    query += `&SortField=${sortField}`;
  }
  if (sortOrder) {
    query += `&SortOrder=${sortOrder}`;
  }
  if (filterField) {
    query += `&FilterField=${filterField}`;
  }
  if (filterOperator) {
    query += `&FilterOperator=${filterOperator}`;
  }
  if (filterValue) {
    query += `&FilterValue=${filterValue || ''}`;
  }
  return http.get(`${baseUrl}/Category/get-active-categories?${query}`).catch(e => console.log('getActiveCategories', e))
}