import * as qs from 'qs';
import { baseUrl } from '../../utils/base-url.constant';
import http from '../../utils/http';

export const getAllLevels = (pageSize, page, sortField = null, sortOrder = null, filterField = null, filterOperator = null, filterValue = null) => {
  const query = qs.stringify({
    PageSize: pageSize,
    PageNumber: page,
    SortField: sortField,
    SortOrder: sortOrder,
    FilterField: filterField,
    FilterOperator: filterOperator,
    FilterValue: filterValue
  }, { skipNulls: true })
  return http.get(`${baseUrl}/Level/get-levels?${query}`, { params: query }).catch(e => console.log('getAllLevels', e))
}

export const getActiveLevel = (pageSize, page, sortField = null, sortOrder = null, filterField = null, filterOperator = null, filterValue = null) => {
  const query = qs.stringify({
    PageSize: pageSize,
    PageNumber: page,
    SortField: sortField,
    SortOrder: sortOrder,
    FilterField: filterField,
    FilterOperator: filterOperator,
    FilterValue: filterValue
  }, { skipNulls: true })
  return http.get(`${baseUrl}/Level/get-active-levels?${query}`, { params: query }).catch(e => console.log('getActiveLevel', e))
}

export const createLevel = (level) => {
  return http.post(`${baseUrl}/Level/create-level`, level).catch(e => console.log('createLevel', e))
}

export const updateLevel = (id, level) => {
  return http.post(`${baseUrl}/Level/update-level/${id}`, level).catch(e => console.log('updateLevel', e))
}