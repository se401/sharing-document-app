import * as qs from 'qs';
import { baseUrl } from '../../utils/base-url.constant';
import http from '../../utils/http';

export const createPost = (post, background) => {
  // const body = {
  //   Title: post.title,
  //   Description: post.description,
  //   CategoryId: post.category,
  //   LevelId: post.level,
  //   ResourceId: post.resource,
  //   Background: background,
  //   Source: source
  // };
  const body = new FormData();
  body.append('Title', post.title);
  body.append('Description', post.description);
  body.append('CategoryId', post.category);
  body.append('LevelId', post.level);
  body.append('ResourceId', post.resource);
  body.append('Background', background);
  body.append('Source', post.source);
  body.append('Price', post.price);

  return http
    .post(`${baseUrl}/Post/create-post`, body, {
      headers: { 'Content-Type': 'multipart/form-data' },
    })
    .catch(e => console.log('createPost', e));
};

export const editPost = (id, post, background) => {
  const body = new FormData();
  body.append('Title', post.title);
  body.append('Description', post.description);
  body.append('CategoryId', post.category);
  body.append('LevelId', post.level);
  body.append('ResourceId', post.resource);
  body.append('Background', background);
  body.append('Source', post.source);
  body.append('Price', post.price);

  return http
    .post(`${baseUrl}/Post/edit/${id}`, body, {
      headers: { 'Content-Type': 'multipart/form-data' },
    })
    .catch(e => console.log('createPost', e));
};

export const getPostById = id => {
  return http.get(`${baseUrl}/Post/get-post/${id}`);
};

export const getPostByCateId = (
  id,
  pageSize,
  page,
  sortField = null,
  sortOrder = null,
  filterField = null,
  filterOperator = null,
  filterValue = null,
) => {
  let query = `PageSize=${pageSize}&PageNumber=${page}`;
  if (sortField) {
    query += `&SortField=${sortField}`;
  }
  if (sortOrder) {
    query += `&SortOrder=${sortOrder}`;
  }
  if (filterField) {
    query += `&FilterField=${filterField}`;
  }
  if (filterOperator) {
    query += `&FilterOperator=${filterOperator}`;
  }
  if (filterValue) {
    query += `&FilterValue=${filterValue || ''}`;
  }

  return http.get(`${baseUrl}/Post/get-posts-cate/${id}?${query}`);
};

export const getPostByAuthorId = (
  id,
  pageSize,
  page,
  sortField = null,
  sortOrder = null,
  filterField = null,
  filterOperator = null,
  filterValue = null,
) => {
  let query = `PageSize=${pageSize}&PageNumber=${page}`;
  if (sortField) {
    query += `&SortField=${sortField}`;
  }
  if (sortOrder) {
    query += `&SortOrder=${sortOrder}`;
  }
  if (filterField) {
    query += `&FilterField=${filterField}`;
  }
  if (filterOperator) {
    query += `&FilterOperator=${filterOperator}`;
  }
  if (filterValue) {
    query += `&FilterValue=${filterValue || ''}`;
  }
  console.log(`${baseUrl}/Post/get-posts-author/${id}?${query}`);
  return http.get(`${baseUrl}/Post/get-posts-author/${id}?${query}`);
};

export const getPostByResourceId = (
  resourceId,
  pageSize,
  page,
  sortField = null,
  sortOrder = null,
  filterField = null,
  filterOperator = null,
  filterValue = null,
) => {
  let query = `PageSize=${pageSize}&PageNumber=${page}`;
  if (sortField) {
    query += `&SortField=${sortField}`;
  }
  if (sortOrder) {
    query += `&SortOrder=${sortOrder}`;
  }
  if (filterField) {
    query += `&FilterField=${filterField}`;
  }
  if (filterOperator) {
    query += `&FilterOperator=${filterOperator}`;
  }
  if (filterValue) {
    query += `&FilterValue=${filterValue || ''}`;
  }
  return http.get(`${baseUrl}/Post/get-posts-resource/${resourceId}?${query}`);
};

export const getActivePosts = (
  pageSize,
  page,
  sortField = null,
  sortOrder = null,
  filterField = null,
  filterOperator = null,
  filterValue = null,
) => {
  let query = `PageSize=${pageSize}&PageNumber=${page}`;
  if (sortField) {
    query += `&SortField=${sortField}`;
  }
  if (sortOrder) {
    query += `&SortOrder=${sortOrder}`;
  }
  if (filterField) {
    query += `&FilterField=${filterField}`;
  }
  if (filterOperator) {
    query += `&FilterOperator=${filterOperator}`;
  }
  if (filterValue) {
    query += `&FilterValue=${filterValue || ''}`;
  }
  return http.get(`${baseUrl}/Post/get-active-posts?${query}`);
};

export const getAllPost = (
  pageSize,
  page,
  sortField = null,
  sortOrder = null,
  filterField = null,
  filterOperator = null,
  filterValue = null,
) => {
  let query = `PageSize=${pageSize}&PageNumber=${page}`;
  if (sortField) {
    query += `&SortField=${sortField}`;
  }
  if (sortOrder) {
    query += `&SortOrder=${sortOrder}`;
  }
  if (filterField) {
    query += `&FilterField=${filterField}`;
  }
  if (filterOperator) {
    query += `&FilterOperator=${filterOperator}`;
  }
  if (filterValue) {
    query += `&FilterValue=${filterValue || ''}`;
  }
  return http.get(`${baseUrl}/Post/get-all-posts?${query}`);
};

export const getApprovedPost = (
  pageSize,
  page,
  sortField = null,
  sortOrder = null,
  filterField = null,
  filterOperator = null,
  filterValue = null,
) => {
  let query = `PageSize=${pageSize}&PageNumber=${page}`;
  if (sortField) {
    query += `&SortField=${sortField}`;
  }
  if (sortOrder) {
    query += `&SortOrder=${sortOrder}`;
  }
  if (filterField) {
    query += `&FilterField=${filterField}`;
  }
  if (filterOperator) {
    query += `&FilterOperator=${filterOperator}`;
  }
  if (filterValue) {
    query += `&FilterValue=${filterValue || ''}`;
  }
  return http.get(`${baseUrl}/Post/get-appr-posts?${query}`, {
    params: query,
  });
};

export const approvePost = id => {
  return http.get(`${baseUrl}/Post/approve/${id}`);
};

export const getReportPost = id => {
  return http.get(`${baseUrl}/Post/report/${id}`);
};

export const likePost = (id, isLike) => {
  return http.post(`${baseUrl}/Post/like`, {
    postId: id,
    isLike: true,
  });
};

export const buyPost = id => {
  return http.post(`${baseUrl}/Post/buy/${id}`, {});
};

export const viewPost = id => {
  return http.post(`${baseUrl}/Post/view/${id}`, {});
};

export const getBookmarkPost = (
  pageSize,
  page,
  sortField = null,
  sortOrder = null,
  filterField = null,
  filterOperator = null,
  filterValue = null,
) => {
  let query = `PageSize=${pageSize}&PageNumber=${page}`;
  if (sortField) {
    query += `&SortField=${sortField}`;
  }
  if (sortOrder) {
    query += `&SortOrder=${sortOrder}`;
  }
  if (filterField) {
    query += `&FilterField=${filterField}`;
  }
  if (filterOperator) {
    query += `&FilterOperator=${filterOperator}`;
  }
  if (filterValue) {
    query += `&FilterValue=${filterValue || ''}`;
  }
  return http.get(`${baseUrl}/Post/get-bookmark-post?${query}`);
};

export const getLikedPost = (
  pageSize,
  page,
  sortField = null,
  sortOrder = null,
  filterField = null,
  filterOperator = null,
  filterValue = null,
) => {
  let query = `PageSize=${pageSize}&PageNumber=${page}`;
  if (sortField) {
    query += `&SortField=${sortField}`;
  }
  if (sortOrder) {
    query += `&SortOrder=${sortOrder}`;
  }
  if (filterField) {
    query += `&FilterField=${filterField}`;
  }
  if (filterOperator) {
    query += `&FilterOperator=${filterOperator}`;
  }
  if (filterValue) {
    query += `&FilterValue=${filterValue || ''}`;
  }
  return http.get(`${baseUrl}/Post/get-liked-posts?${query}`);
};

export const getUrl = postId => {
  return http.get(`${baseUrl}/Post/get-url/${postId}`);
};


