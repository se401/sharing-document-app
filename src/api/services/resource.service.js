import { baseUrl } from '../../utils/base-url.constant';
import * as qs from 'qs';
import http from '../../utils/http';


export const getAllResources = (pageSize, page, sortField = null, sortOrder = null, filterField = null, filterOperator = null, filterValue = null) => {
  const query = qs.stringify({
    PageSize: pageSize,
    PageNumber: page,
    SortField: sortField,
    SortOrder: sortOrder,
    FilterField: filterField,
    FilterOperator: filterOperator,
    FilterValue: filterValue
  }, { skipNulls: true })
  return http.get(`${baseUrl}/Resource/get-resources?${query}`, { params: query }).catch(e => console.log('getAllResources', e))
}

export const getActiveResource = (pageSize, page, sortField = null, sortOrder = null, filterField = null, filterOperator = null, filterValue = null) => {
  const query = qs.stringify({
    PageSize: pageSize,
    PageNumber: page,
    SortField: sortField,
    SortOrder: sortOrder,
    FilterField: filterField,
    FilterOperator: filterOperator,
    FilterValue: filterValue
  }, { skipNulls: true })
  return http.get(`${baseUrl}/Resource/get-active-resources?${query}`, { params: query }).catch(e => console.log('getActiveResource', e))
}

export const createResource = (Resource) => {
  return http.post(`${baseUrl}/Resource/create-resource`, Resource).catch(e => console.log('createResource', e))
}

export const updateResource = (id, Resource) => {
  return http.post(`${baseUrl}/Resource/update-resource/${id}`, Resource).catch(e => console.log('updateResource', e))
}