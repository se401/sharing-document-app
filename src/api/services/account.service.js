import { baseUrl } from '../../utils/base-url.constant';
import http from '../../utils/http';

export const updateAccountInfo = (address, displayName) => {
  return http
    .post(`${baseUrl}/User/update-account-info`, { address, displayName })
    .catch(e => console.log('updateAccountInfo', e));
};

export const changeAvatar = file => {
  const body = new FormData();
  body.set('file', file);

  return http
    .post(`${baseUrl}/User/change-avatar`, body, {
      headers: { 'Content-Type': 'multipart/form-data' },
    })
    .catch(e => console.log('changeAvatar', e));
};

export const changePassword = (password, confirmPassword) => {
  console.log(
    'LOG ~ file: account.service.js ~ line 19 ~ changePassword ~ confirmPassword',
    confirmPassword,
  );
  console.log(
    'LOG ~ file: account.service.js ~ line 19 ~ changePassword ~ password',
    password,
  );
  return http
    .post(`${baseUrl}/User/change-password`, { password, confirmPassword })
    .catch(e => console.log('changePassword', e));
};

export const getUsers = () => {
  return http
    .get(`${baseUrl}/User/get-users`)
    .catch(e => console.log('getUsers', e));
};

export const getUserById = id => {
  return http
    .get(`${baseUrl}/User/get-user/${id}`)
    .catch(e => console.log('getUserById', e));
};

export const getUserInfo = () => {
  return http
    .get(`${baseUrl}/User/get-user-info`)
    .catch(e => console.log('getUserInfo', e));
};

export const saveBookMart = postId => {
  return http
    .post(`${baseUrl}/User/save-bookmark/${postId}`)
    .catch(e => console.log('saveBookMart', e));
};

export const removeBookMart = postId => {
  return http
    .post(`${baseUrl}/User/remove-bookmark/${postId}`)
    .catch(e => console.log('removeBookMart', e));
};

export const getBalance = () => {
  return http.get(`${baseUrl}/User/get-eth-balance`);
};

export const generateKey = roles => {
  return http.post(`${baseUrl}/User/gen-eth-api-key`, {
    roles: roles,
  });
};

export const getTransactions = (
  pageSize,
  page,
  sortField = null,
  sortOrder = null,
  filterField = null,
  filterOperator = null,
  filterValue = null,
) => {
  let query = `PageSize=${pageSize}&PageNumber=${page}`;
  if (sortField) {
    query += `&SortField=${sortField}`;
  }
  if (sortOrder) {
    query += `&SortOrder=${sortOrder}`;
  }
  if (filterField) {
    query += `&FilterField=${filterField}`;
  }
  if (filterOperator) {
    query += `&FilterOperator=${filterOperator}`;
  }
  if (filterValue) {
    query += `&FilterValue=${filterValue || ''}`;
  }
  return http.get(`${baseUrl}/User/get-transactions`);
};

export const transfer = (address, amount) => {
  return http.post(`${baseUrl}/User/transfer`, {
    address: address,
    amount: amount,
  });
};

export const getTokenReward = () => {
  return http.get(`${baseUrl}/User/get-token-reward`);
};

export const reward = (token, amount = 0.01) => {
  return http.post(`${baseUrl}/User/reward`, {
    token: token,
    amount: amount,
  });
};