import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { StatusBar } from 'react-native';
import { AppearanceProvider, useColorScheme } from 'react-native-appearance';
import getTheme from '../../theme';
import ChangePassword from '../screens/ChangePassword/ChangePassword';
import Loading from '../screens/Loading/Loading';
import Login from '../screens/Login/Login';
import BookDetail from '../screens/Main/Book/BookDetail';
import BookDetailV2 from '../screens/Main/Book/BookDetailV2';
import BookReading from '../screens/Main/Book/BookReading';
import BookSearch from '../screens/Main/Book/BookSearch';
import CreatePost from '../screens/Main/Book/CreatePost';
import MainRouter from '../screens/Main/Main';
import AboutApp from '../screens/Main/Profile/AboutApp/AboutApp';
import Info from '../screens/Main/Profile/Info/Info';
import Verify from '../screens/Main/Verify/Verify';
import Register from '../screens/Register/Register';


const Stack = createStackNavigator();

const config = {
  animation: 'spring',
  config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};

const MyRouter = () => {
  const scheme = useColorScheme();
  return (
    <AppearanceProvider>
      <NavigationContainer theme={getTheme(scheme)}>
        <StatusBar backgroundColor={'#f78a37'} />
        <Stack.Navigator initialRouteName={'Login'}>
          <Stack.Screen name='Login' component={Login} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name='Verify' component={Verify} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="Main" component={MainRouter} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="Register" component={Register} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="AboutApp" component={AboutApp} options={{ headerTitle: 'About SharingApp', headerShown: true, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="Loading" component={Loading} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="BookDetail" component={BookDetail} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="BookDetailV2" component={BookDetailV2} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="BookSearch" component={BookSearch} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="BookReading" component={BookReading} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="CreatePost" component={CreatePost} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="ChangePassword" component={ChangePassword} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
          <Stack.Screen name="Info" component={Info} options={{ headerShown: false, cardOverlayEnabled: true, cardStyle: { backgroundColor: 'transparent' } }} />
        </Stack.Navigator>
      </NavigationContainer>
    </AppearanceProvider>

  )
}
export default MyRouter