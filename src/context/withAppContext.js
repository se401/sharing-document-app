import { Component } from 'react';

export const withAppContext = Component => props => (
  <FirebaseConsumer>
    {state => <Component {...props} app={state} />}
  </FirebaseConsumer>
)