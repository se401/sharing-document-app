import {
  BannerAd,
  BannerAdSize,
  RewardedAd,
  RewardedAdEventType,
  TestIds,
} from '@react-native-firebase/admob';
import React, { createContext, useState } from 'react';
const banner_id = __DEV__
  ? TestIds.BANNER
  : 'ca-app-pub-5145748815818405/8210092854';
const reward_id = __DEV__
  ? TestIds.REWARDED
  : 'ca-app-pub-5145748815818405/3177258026';
export const AdmobContext = createContext();
export const AdmobController = ({ children }) => {
  const [point, setPoint] = useState();
  const initRewardAds = () => {
    const rewarded = RewardedAd.createForAdRequest(reward_id, {
      requestNonPersonalizedAdsOnly: true,
    });
    rewarded.onAdEvent(async (type, error, reward) => {
      if (type === RewardedAdEventType.LOADED) {
        rewarded.show();
      }
      if (type === RewardedAdEventType.EARNED_REWARD) {
        console.log('rewarded.onAdEvent ~ reward', reward);
      }
    });

    rewarded.load();
  };

  const renderBanner = () => {
    return (
      <BannerAd
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
        }}
        requestOptions={{keywords: ['travel']}}
        unitId={banner_id}
        size={BannerAdSize.SMART_BANNER}
        onAdLoaded={() => {
          console.log('Advert loaded');
        }}
        onAdFailedToLoad={error => {
          console.log('Advert failed to load: ', error);
        }}
      />
    );
  };
  return (
    <AdmobContext.Provider
      value={{ renderBanner, initRewardAds, point, setPoint }}>
      {children}
    </AdmobContext.Provider>
  );
};
