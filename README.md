# Ứng dụng Chia sẻ tài liệu học tập - Nhóm BCD
- Đồ án môn học: Chuyên đề Mobile and Pervasive
- Lớp: SE405.I21
- Giáo viên: Mr. Huỳnh Tuấn Anh [Github](https://github.com/anhhna/ "Github")
- Apk file: [Download](https://drive.google.com/file/d/1_zjxCBjGTGBx0W1FX5LYCIcM7leO5Qgw/view?usp=sharing)
## Giới thiệu :
- Ứng dụng còn tạo ra nơi lưu trữ các nguồn tài liệu học tập (IT) hỗ trợ cho các bạn sinh viên mới bắt đầu học lâoj trình.
## Thành viên nhóm :
- 16520085: Nguyễn Ngọc Duy Bảo
- 16520246: Phạm Thanh Đức
- 16520131: Nguyễn Thành Công
## Chức năng :
- Xem và tìm kiếm thông tin tài liệu
- Chia sẻ tài liệu thông qua google drive
- Hỗ trợ thanh toán cho những tài liệu cao cấp (blockchain)
- Hỗ trợ quảng cáo giúp kiếm được phí thanh toán
- Hỗ trợ quảng lý thông tin (đổi thông tin, mật khẩu)
## Thư viện và công nghệ:
- React native: [Reference](https://reactnative.dev/docs/getting-started)
- Ganache [Reference](https://www.trufflesuite.com/docs/ganache/overview)
- APS.Net Core [Reference](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-5.0)
- Angular [Reference](https://angular.io/docs)
- AWS [Reference](https://docs.aws.amazon.com/)
- MongoDb [Referenc](https://docs.mongodb.com/manual/)
## Backend/Database/Diagram
- API detail: [Swagger](http://16.162.161.43:5000/swagger/index.html)
- API detail for Transaction: [Swagger](http://16.162.161.43:9999/api-docs)
## Giao diện
- Trang chủ<space><space>
![Trang chủ](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/home.png)
- Danh mục<space><space>
![Danh mục](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/category.png)
- Nguồn<space><space>
![Nguồn](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/resource.png)
- Bài đăng<space><space>
![Bài đăng](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/mytab.png)
- Cá nhân<space><space>
![Cá nhân](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/profile.png)
- Hoạt động<space><space>
![Hoạt động](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/activity.png)
- Đổi mật khẩu<space><space>
![Đổi mật khẩu](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/changepassword.png)
- Bình luận<space><space>
![Bình luận](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/cmt.png)
- Đăng tài liệu<space><space>
![Đăng tài liệu](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/create.png)
- Chi tiết<space><space>
![Chi tiết](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/detail.png)
- Chia sẻ<space><space>
![Chia sẻ](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/sahre.png)
- Tìm kiếm<space><space>
![Tìm kiếm](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/search.png)
- Xem quảng cáo<space><space>
![Xem quảng cáo](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/watch.png)
- Thông tin app<space><space>
![Thông tin app](https://gitlab.com/se401/sharing-document-app/-/raw/master/assets/about.png)
## Owner
- 16520085: Nguyễn Ngọc Duy Bảo
- 16520246: Phạm Thanh Đức
- 16520131: Nguyễn Thành Công
## License [MIT](https://www.mit.edu/~amini/LICENSE.md)




