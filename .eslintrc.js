module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ['react', 'react-native', 'import'],
  extends: [
    'eslint:recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:react/recommended',
    'plugin:react-native/all',
    'airbnb-base',
  ],
  rules: {
    'class-methods-use-this': 'off',
    'no-use-before-define': 'off',
    'object-curly-newline': 'off',
    'no-unused-vars': 'off',
    'no-prototype-builtins': 'off',
    'no-restricted-imports': 'off',
    'import/no-unresolved': 'off',
    'import/extensions': 'off',
    'react-native/no-color-literals': 'off',
    'no-undef': 'warn',
    'comma-dangle': 'off',
    'linebreak-style': ["error", "windows", "unix"]
  },
  settings: {
    'import/resolver': {
      'babel-plugin-root-import': {
        rootPathPrefix: '~',
        rootPathSuffix: 'src',
      },
    },
  },
  env: {
    jest: true,
  },
};
