/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import messaging from '@react-native-firebase/messaging';
import React, { useContext, useEffect, useState } from 'react';
import {
  Alert,
  PermissionsAndroid,
  Platform,
  useColorScheme,
} from 'react-native';
import {
  setJSExceptionHandler,
  setNativeExceptionHandler,
} from 'react-native-exception-handler';
import 'react-native-gesture-handler';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { AdmobController } from './src/context/AdmobContext';
import { AppProvider } from './src/context/AppContext';
import MyRouter from './src/routers/index';
import { errorHandler } from './src/utils/error';

setJSExceptionHandler(errorHandler, true);

setNativeExceptionHandler(errorString => {
  console.log('setNativeExceptionHandler', errorString);
});

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [weather, setWeather] = useState(null);

  const requestLocationPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          {
            title: 'Location Write Permission',
            message: 'App needs write permission',
          },
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert('Write permission err', err);
      }
      return false;
    } else return true;
  };

  useEffect(() => {
    requestLocationPermission();
  }, []);

  useEffect(() => {
    // Assume a message-notification contains a "type" property in the data payload of the screen to open
    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log('messaging ~ remoteMessage', remoteMessage);
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
    });

    console.log('useEffect ~ messaging().app.options', messaging().app.options);

    messaging()
      .getToken()
      .then(token => {
        console.log('useEffect ~ token', token);
      });

    messaging()
      .requestPermission()
      .then(val => {
        console.log('useEffect ~ requestPermission ~ val', val);
      });

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log('useEffect ~ remoteMessage', remoteMessage);
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
        }
      });
  }, []);

  // GetLocation.getCurrentPosition({
  //   enableHighAccuracy: true,
  //   timeout: 15000,
  // })
  //   .then(async location => {
  //     console.log(location);
  //     // const data = (await getByCoordinates(location.latitude, location.latitude)).data
  //     // setWeather(data.weather.map(w => w.description).join(','));
  //   })
  //   .catch(error => {
  //     const { code, message } = error;
  //     console.warn(code, message);
  //   })

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <AdmobController>
      <AppProvider value={{ isLogin: false, weather: weather, count: 1 }}>
        <MyRouter />
      </AppProvider>
    </AdmobController>
  );
};

export default App;
